<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\wfh;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class WfhController extends BaseController
{
    public function mingguan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggalawal' => 'required',
            'tanggalakhir' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $startDate = $input['tanggalawal'];
        $endDate = $input['tanggalakhir'];
  
        $wfh = wfh::select('*')
                        ->whereDate('created_at', '>=', $startDate)
                        ->whereDate('created_at', '<=', $endDate)
                        ->get();
        if(!empty($wfh)){

        
        $response=array(
            'wfh'=>$wfh
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200); 
        
    }
}
