<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\wfhjam;
use App\Models\wfh;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class WfhjamController extends BaseController
{
    public function list(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggal' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $tanggal = $input['tanggal'];
  
        $wfh = wfhjam::select('*')
                        ->whereDate('tanggal', '>=', $tanggal)
                        ->get();
        if(!empty($wfh)){

        
        $response=array(
            'listwfh'=>$wfh
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200); 
        
    }
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggal' => 'required',
            'id_cek'=>'required',
            'jenis' => 'required',
            'tanggal' => 'required',
            'waktu' => 'required',
            'eksekusi' => 'required',
            
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $cek=wfhjam::select('*')
        ->whereDate('tanggal', '=', $input['tanggal'])
        ->where('id_cek',$input['id_cek'])
        ->first();
        if(empty($cek)){
        $wfh = wfhjam::insert($input);
        }
        else{
            $wfh=DB::table('wfhjams')
            ->where('id_cek', $input['id_cek'])
            ->update(['eksekusi' => $input['eksekusi']]);
        }
        $response=array(
            'success'=>true
        );
     
        return response()->json($response,200); 
    }
    public function stop(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggal' => 'required',
            'id_cek'=>'required',
            'jenis' => 'required',
            'tanggal' => 'required',
            'waktu' => 'required',
            'eksekusi' => 'required',
            
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $cek=wfhjam::select('*')
        ->whereDate('tanggal', '=', $input['tanggal'])
        ->where('id_cek',$input['id_cek'])
        ->first();
        if(empty($cek)){
        $wfh = wfhjam::insert($input);
        }
        else{
            $wfh=DB::table('wfhjams')
            ->where('id_cek', $input['id_cek'])
            ->update(['eksekusi' => $input['eksekusi']]);
        }
        if(!empty($wfh)){
            $min=wfhjam::select('waktu','jenis')
            ->where('tanggal',$input['tanggal'])
            ->where('id_user',$input['id_user'])
            ->where('jenis',$input['jenis'])
            ->orderBy('waktu','ASC')
            ->first();
            $max=wfhjam::select('waktu','jenis')
            ->where('tanggal',$input['tanggal'])
            ->where('id_user',$input['id_user'])
            ->where('jenis',$input['jenis'])
            ->orderBy('waktu','DESC')
            ->first();
            if(!empty($min) && !empty($max)){
                $time1 = strtotime($min['waktu']);
                $time2 = strtotime($max['waktu']);
                $difference = sprintf ("%.1f", round(abs($time2 - $time1) / 3600,2));
                if(!empty($difference)){
                    $cekwfh=wfh::select('*')
                    ->whereDate('created_at', '=', $input['tanggal'])
                    ->where('id_user',$input['id_user'])
                    ->first();
                    if(empty($cekwfh)){
                        $data=array(
                            'id_user'=>$input['id_user'],
                            'jenis'=>$input['jenis'],
                            'jam'=>$difference
                        );
                        $wfh=wfh::insert($data);
                    }
                    else{
                        $data=array(
                            'jenis'=>$input['jenis'],
                            'jam'=>$difference
                        );
                        $wfh=DB::table('wfhs')
                        ->where('id_wfh', $cekwfh['id_wfh'])
                        ->update($data);
                    }
                }
            }
            else
            {
                $response=array(
                    'message'=>'Gagal Upload'
                );
            }
            
        $response=array(
            'success'=>true
        );
        }
        else{
            $response=array(
                'message'=>'Gagal Upload'
            );
        }   
        return response()->json($response,200); 
    }
    public function getid(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggal' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $wfh=wfhjam::select('*')
        ->whereDate('tanggal', '=', $input['tanggal'])
        ->orderBy('id_cek', 'DESC')
        ->first();
        if(!empty($wfh)){
        $response=array(
            'getid'=>$wfh
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200);
    }
}
