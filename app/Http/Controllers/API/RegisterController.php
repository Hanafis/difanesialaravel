<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use App\Models\user_detail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\Reset;
use Validator;

   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
   
        $input = $request->all();
        $register['name']=$input['name'];
        $register['email']=$input['email'];
        $register['password'] = bcrypt($input['password']);
        $user = User::create($register);
        $data['id_user']=$user->id;
        $data['nama']=$user->name;
        $data['email']=$user->email;
        $detail= user_detail::create($data);
        $respone=[
            'success'=>true,
            'message'=>'Berhasil Registrasi',
            'token'    =>$user->createToken('MyApp')->accessToken,
            'name'     =>$user->name,
            'id_user'        =>$user->id,
            'email'     =>$user->email,
            'image'     =>null,
            'level'     =>0,
        ];
        $login=[
            'Login'=>$respone
        ];
        return response()->json($login,200); 
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            
            $user = Auth::user();
            $detail= user_detail::select('*')
            ->where('id_user',$user->id)
            ->first();
            if(!empty($detail)){
             
            $respone=[
                'success'   =>true,
                'message'   =>'Berhasil Registrasi',
                'token'     =>$user->createToken('MyApp')->accessToken,
                'name'      =>$detail['name'],
                'id_user'   =>$user->id,
                'email'     =>$user->email,
                'image'     =>url('/storage/user/'.$user->image),
                'level'     =>$user->level
            ];
            $login=[
                'Login'=>$respone
            ];
            return response()->json($login,200); 
            }
            
        } 
        else{ 
            $respone=[
                'success'=>true,
                'message'=>'User & Password Yang Anda Masukan Salah',
            ];
            return response()->json($respone,200); 
        } 
    }
    public function gagal(){
        return response()->json('Invalid Token', 401);
    }

    public function userupdate(Request $request,$id){

        $input = $request->all();
        if(!empty($input['tanggal_lahir'])){
        $input['tanggal_lahir']=date("Y-m-d", strtotime($input['tanggal_lahir']));
        }
        $cek=$name=DB::table('user_details')
        ->select('id_user')
        ->where('id_user', $id)
        ->value('id_user');
        $image = $request->file('image');
        if(!empty($image))
        {
            $tags = explode(",", $request->tags);

            $image = new Image;
            $getImage = $request->image;
            $imageName = time().'.'.$getImage->extension();
            $imagePath = public_path(). '/storage/user';
    
            $image->path = $imagePath;
            $image->image = $imageName;
            $dataimg=$imageName;
            $getImage->move($imagePath, $imageName);
            
            $Images=DB::table('users')
            ->where('id', $id)
            ->update(['gambar' => $imageName],);
        }
        if(empty($cek)){
            $input['id_user']=$id;
            if(!empty($dataimg)){
            $input['image']=$dataimg;
            }
            $updateuser = user_detail::create($input);
        }
        else {
            if(!empty($dataimg)){
                $input['image']=$dataimg;
                }
            $updateuser = DB::table('user_details')
            ->where('id_user', $id)
            ->update($input);
        }
        if(!empty($input['name'])){
            $name=DB::table('users')
            ->where('id', $id)
            ->update(['name' => $input['name']]);
        }
        
        $respone=[
            'message'=>'Sukses User Berhasil DI Update'.$id
        ];
        return response()->json($respone,200);
    }
    
    public function user(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user'=>'required',
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $forums = DB::table('user_details')
        ->select('user_details.*')
        ->where('id_user',$input['id_user'])
        ->join('users', 'user_details.id_user', '=', 'users.id')
        ->first();
        if(!empty($forums)){
            $respone=[
                'User'  =>$forums,
            ];
            return response()->json($respone,200);
        }

    }
    public function reset(Request $request) {
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $user = DB::table('users')
        ->select('id')
        ->where('email',$input['email'])
        ->first();
        
        if(!empty($user)){
            $randomid = mt_rand(100000,999999);
            $kode=DB::table('users')
            ->where('email', $input['email'])
            ->update(['kode' => $randomid]);
                $respone=[
                    'message'  =>"Kode Berhasil Dikirim Ke Email,".$input['email'],
                ];
                Mail::to($input['email'])->send(new Reset($randomid));
                return response()->json($respone,200);
        }
    }
    
    public function kode(Request $request){
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'kode'=>'required'
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $user = DB::table('users')
        ->select('id')
        ->where('email',$input['email'])
        ->where('kode',$input['kode'])
        ->first();
        if(!empty($user)){
            $respone=[
                'success'  =>"Benar",
            ];
            
    }
    else{
        $respone=[
            'message'  =>"Kode Yang Anda Masukan Salah",
        ];
    }
    return response()->json($respone,200);
    }
    public function password(Request $request){
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'kode'=>'required',
            'password'=>'required',
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $password=bcrypt($input['password']);
        $user = DB::table('users')
        ->select('id')
        ->where('email',$input['email'])
        ->where('kode',$input['kode'])
        ->first();
        
        if(!empty($user)){
            $reset=DB::table('users')
            ->where('email', $input['email'])
            ->update(['kode' => null,'password'=>$password]);
            $respone=[
                'message'  =>"Berhasil",
            ];
            return response()->json($respone,200);
        }
    
    }
    public function ubah_password(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user'=>'required',
            'old_password'=>'required',
            'newpassword'=>'required',
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $hashedPassword = Auth::user()->password;
 
       if (\Hash::check($request->old_password , $hashedPassword )) {
 
         if (!\Hash::check($request->newpassword , $hashedPassword)) {
            $new=bcrypt($input['newpassword']);
            $reset=DB::table('users')
            ->where('id', $input['id_user'])
            ->update(['password' => $new]);
            $respone=[
                'message'  =>"Berhasil",
            ];
            return response()->json($respone,200);
        }
        else{
            $respone=[
                'message'  =>"Password Baru Tidak Boleh Sama dengan Password Lama",
                'password'=>$password
            ];
            return response()->json($respone,200);
            }
        
        }
        else{
            $respone=[
                'message'  =>"Password Lama Yang Anda Masukan Salah",
                'password'=>$password
            ];
            return response()->json($respone,200);
        }
    }
    
}