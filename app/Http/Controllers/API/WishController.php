<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Wish;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class WishController extends BaseController
{
    public function list(Request $request){
        $validator = Validator::make($request->all(), [
            'start'=>'required',
            'limit'=>'required',
            'id_user'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $product=DB::table('wishes');
        $product->select('products.*','id_wish',
       
        DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=wishes.id_user and alamat_users.utama='ya' limit 1) as kota"),
        DB::raw("(select pathimage from product_images where product_images.id_product=wishes.id_product and product_images.sampul='ya' limit 1) as image"),
        DB::raw("(select AVG(rating) from ratings where ratings.id_product=wishes.id_product) as rating"))
        ->join('products','wishes.id_product','=','products.id_product')
        ->where('wishes.id_user',$input['id_user']);
        if(!empty($input['keyword'])){
        $product=$product->where('products.nama','LIKE','%'.$input['keyword'].'%');
        }
        $product=$product->orderBy('wishes.created_at','DESC')
        ->limit($input['limit'])->offset($input['start'])
        ->get();
        
        $request=[
            'Produk'=>$product
        ];
        return response()->json($request,200); 
   
        
    }
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_product'=>'required',
            'id_user'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $data=DB::table('wishes')
        ->select('*')
        ->where('id_user',$input['id_user'])
        ->where('id_product',$input['id_product'])
        ->first();
        if(empty($data)){
        $wish=Wish::create($input);
        $request=[
            'message'=>"Produk Telah Dimasukan Ke Daftar Favorit",
            'success'=>true
        ];
        return response()->json($request,200); 
        }
        else{
        $keranjang=DB::table('wishes')
        ->where('id_product',$input['id_product'])
        ->where('id_user',$input['id_user'])
        ->delete();
        if(!empty($keranjang)){
            $request=[
                'message'=>"Produk Telah Dihapus Dari Daftar Favorit",
                'success'=>false
            ];
            return response()->json($request,200); 
        }
        }


    }
 
}
