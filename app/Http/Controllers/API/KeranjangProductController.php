<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\KeranjangProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Validator;

class KeranjangProductController extends BaseController
{
    //
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'id_product'=>'required',
            'jml'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $data=DB::table('keranjang_products')
        ->select('*')
        ->where('id_user',$input['id_user'])
        ->where('id_product',$input['id_product'])
        ->first();
        if(empty($data)){
        $keranjang=KeranjangProduct::create($input);
        }
        else{
            
            $keranjang=DB::table('keranjang_products')
            ->where('id_keranjang',$data->id_keranjang)
            ->update(['jml' => $data->jml+$input['jml']]);
        }
        if(!empty($keranjang)){
            $request=[
                'message'=>"Produk Telah Dimasukan Ke Keranjang"
            ];
            return response()->json($request,200); 
        }

    }
    public function kurang(Request $request){
        $validator = Validator::make($request->all(), [
            'id_keranjang' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $data=DB::table('keranjang_products')
        ->select('*')
        ->where('id_keranjang',$input['id_keranjang'])
        ->first();
        if(!empty($data)){
            if($data->jml>1){
                    $keranjang=DB::table('keranjang_products')
                    ->where('id_keranjang',$data->id_keranjang)
                    ->update(['jml' => $data->jml-1]);
            }
            else{
                $keranjang=DB::table('keranjang_products')
                ->where('id_keranjang',$input['id_keranjang'])
                ->delete();
            }
        }
        if(!empty($keranjang)){
            $request=[
                'message'=>"Produk Telah Dimasukan Ke Keranjang"
            ];
            return response()->json($request,200); 
        }

    }
    public function hapus(Request $request){
        $validator = Validator::make($request->all(), [
            'id_keranjang' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $keranjang=DB::table('keranjang_products')
        ->where('id_keranjang',$input['id_keranjang'])
        ->delete();
        if(!empty($keranjang)){
            $request=[
                'message'=>"Produk Telah Dihapus Dari Keranjang"
            ];
            return response()->json($request,200); 
        }
    }
    public function list(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $keranjang=DB::table('keranjang_products')
        ->select('keranjang_products.*','wishes.id_wish','product_varians.id_varian','product_varians.varian','product_varians.harga as varian_harga','product_varians.stock as varian_stock','product_varians.pathimage as varian_image','user_details.name as toko','products.nama','products.stock','products.harga','user_details.id_user as id_toko',
       
        DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=keranjang_products.id_user and alamat_users.utama='ya' limit 1) as kota"),
        DB::raw("(select pathimage from product_images where product_images.id_product=keranjang_products.id_product and product_images.sampul='ya' limit 1) as image"),
        DB::raw("(select AVG(rating) from ratings where ratings.id_product=keranjang_products.id_product) as rating"))
        ->join('products','keranjang_products.id_product','=','products.id_product')
        ->join('user_details','products.id_user','=','user_details.id_user')
        ->leftJoin('wishes','products.id_product','=','wishes.id_product')
        ->leftJoin('product_ongkirs','products.id_product','=','product_ongkirs.id_product')
        ->leftJoin('product_varians','keranjang_products.id_varian','=','product_varians.id_varian')
        ->where('keranjang_products.id_user',$input['id_user'])
       ->orderBy('keranjang_products.created_at','DESC')
        ->get();
        if(!empty($keranjang)){
            $request=[
                'Produk'=>$keranjang
            ];
            return response()->json($request,200); 
        }
    }
    public function count(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $keranjang=DB::table('keranjang_products')
        ->select('keranjang_products.*')
        ->where('keranjang_products.id_user',$input['id_user'])
        ->count();
        if(!empty($keranjang)){
            $request=[
                'JmlKeranjang'=>$keranjang
            ];
            return response()->json($request,200); 
        }
    }
}
