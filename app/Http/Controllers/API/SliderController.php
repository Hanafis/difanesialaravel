<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Validator;

class SliderController extends BaseController
{
    //
    public function index(){
        $slider = Slider::all();
        $response=array();
        foreach ($slider as $s) {
        $response[]=array(
            "url"   =>$s->url,
            "hdpi"  => url('/storage/upload/'.$s->hdpi),
            'mdpi'  => url('/storage/upload/'.$s->mdpi),
            'xhdpi'  => url('/storage/upload/'.$s->xhdpi),
            'xxhdpi'  => url('/storage/upload/'.$s->xxhdpi),
            'xxxhdpi'  => url('/storage/upload/'.$s->xxxhdpi)
            );
            
        }
        $akhir=[
            'Slider'=>$response
        ]; 
        if (!$slider->isEmpty()){
            return response()->json($akhir, 200);
            }
            else {
                return $this->sendRes('Tidak Ada Data');
            } 
    }
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user'=>'required',
            'banner'=>'required'
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        if($request->banner) {

            $image = $request->file('banner');
            $input['banner'] = time().$image->getClientOriginalName();
         
            $destinationPath = public_path('/storage/upload/');
            $img = Image::make($image->path());
            $img->resize(480, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."mdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(800, 480, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."hdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(960, 640, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xhdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(1600, 960, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xxhdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(1920, 960, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xxxhdpi-".$input['banner']);


            $destinationPath = public_path('/storage/upload');
            $image->move($destinationPath, $input['banner']);
            $data['mdpi']= "mdpi-".$input['banner'];
            $data['hdpi']="hdpi-".$input['banner'];
            $data['xhdpi']="xhdpi-".$input['banner'];
            $data['xxhdpi']="xxhdpi-".$input['banner'];
            $data['xxxhdpi']="xxxhdpi-".$input['banner'];
            
            $Images=slider::create($data);
            
            
        }
    }
    public function edit(Request $request){

    }
    
    // public function Banner(Request $request){
    //     $validator = Validator::make($request->all(), [
    //         'id_user'=>'required'
            
    //     ]);
    //     if($validator->fails()){
    //         return response()->json($validator->errors(),200);       
    //     }
    //     $input = $request->all();

    //     $cek=$name=DB::table('users')
    //     ->select('level')
    //     ->where('id', $input['id_user'])
    //     ->value('level');
    //     if($cek=='admin'){
    //         if($request->banner1) {

    //             $image = $request->file('banner1');
    //             $input['banner1'] = time().'banner1.'.$image->extension();
             
    //             $destinationPath = public_path('/storage/images/banner/');
    //             $img = Image::make($image->path());
    //             $img->resize(480, 320, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."mdpi-".$input['banner1']);
    //             $img = Image::make($image->path());
    //             $img->resize(800, 480, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."hdpi-".$input['banner1']);
    //             $img = Image::make($image->path());
    //             $img->resize(960, 640, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xhdpi-".$input['banner1']);
    //             $img = Image::make($image->path());
    //             $img->resize(1600, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxhdpi-".$input['banner1']);
    //             $img = Image::make($image->path());
    //             $img->resize(1920, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxxhdpi-".$input['banner1']);


    //             $destinationPath = public_path('/storage/images');
    //             $image->move($destinationPath, $input['banner1']);

    //             $Images=DB::table('sliders')
    //             ->where('id_slider', '1')
    //             ->update([
    //                 'mdpi' => "mdpi-".$input['banner1'],
    //                 'hdpi'=>"hdpi-".$input['banner1'],
    //                 'xhdpi'=>"xhdpi-".$input['banner1'],
    //                 'xxhdpi'=>"xxhdpi-".$input['banner1'],
    //                 'xxxhdpi'=>"xxxhdpi-".$input['banner1']],);
                
    //         }
    //         if($request->banner2) {

    //             $image = $request->file('banner2');
    //             $input['banner2'] = time().'banner2.'.$image->extension();
             
    //             $destinationPath = public_path('/storage/images/banner/');
    //             $img = Image::make($image->path());
    //             $img->resize(480, 320, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."mdpi-".$input['banner2']);
    //             $img = Image::make($image->path());
    //             $img->resize(800, 480, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."hdpi-".$input['banner2']);
    //             $img = Image::make($image->path());
    //             $img->resize(960, 640, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xhdpi-".$input['banner2']);
    //             $img = Image::make($image->path());
    //             $img->resize(1600, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxhdpi-".$input['banner2']);
    //             $img = Image::make($image->path());
    //             $img->resize(1920, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxxhdpi-".$input['banner2']);


    //             $destinationPath = public_path('/storage/images');
    //             $image->move($destinationPath, $input['banner2']);
    //             $Images=DB::table('sliders')
    //             ->where('id_slider', '2')
    //             ->update([
    //                 'mdpi' => "mdpi-".$input['banner2'],
    //                 'hdpi'=>"hdpi-".$input['banner2'],
    //                 'xhdpi'=>"xhdpi-".$input['banner2'],
    //                 'xxhdpi'=>"xxhdpi-".$input['banner2'],
    //                 'xxxhdpi'=>"xxxhdpi-".$input['banner2']],);
    //         }
    //         if($request->banner3) {

    //             $image = $request->file('banner3');
    //             $input['banner3'] = time().'banner3.'.$image->extension();
             
    //             $destinationPath = public_path('/storage/images/banner/');
    //             $img = Image::make($image->path());
    //             $img->resize(480, 320, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."mdpi-".$input['banner3']);
    //             $img = Image::make($image->path());
    //             $img->resize(800, 480, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."hdpi-".$input['banner3']);
    //             $img = Image::make($image->path());
    //             $img->resize(960, 640, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xhdpi-".$input['banner3']);
    //             $img = Image::make($image->path());
    //             $img->resize(1600, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxhdpi-".$input['banner3']);
    //             $img = Image::make($image->path());
    //             $img->resize(1920, 960, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->save($destinationPath.'/'."xxxhdpi-".$input['banner3']);


    //             $destinationPath = public_path('/storage/images');
    //             $image->move($destinationPath, $input['banner3']);
    //             $Images=DB::table('sliders')
    //             ->where('id_slider', '3')
    //             ->update([
    //                 'mdpi' => "mdpi-".$input['banner3'],
    //                 'hdpi'=>"hdpi-".$input['banner3'],
    //                 'xhdpi'=>"xhdpi-".$input['banner3'],
    //                 'xxhdpi'=>"xxhdpi-".$input['banner3'],
    //                 'xxxhdpi'=>"xxxhdpi-".$input['banner3']],);
    //         }
    //           $response=[
    //               "message"=>"Sukses"
    //           ];
    //         return response()->json($response,200); 
          
    //     }
    //     else{
    //         return response()->json("Error",200);     
    //     }
    // }
    public function createThumbnail($path, $width, $height)
{
    // $img = Image::make($path)->resize($width, $height, function ($constraint) {
    //     $constraint->aspectRatio();
    // });
    $img = Image::make($path)->resize($width, $height)->save($path);
    $img->save($path);
}
}
