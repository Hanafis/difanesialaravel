<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use App\Models\Product;
use App\Models\user_detail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class ProductController extends BaseController
{

    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'pathimage'=>'required',
            'nama' => 'required',
            'harga' => 'required',
            'deskripsi'=>'required',
            'stock'=>'required',
            'kondisi'=>'required',
            'berbahaya'=>'required',
            'jenis'=>'required',
            'varian'=>'required',
            'hargavarian'=>'required',
            'stockvarian'=>'required',
            'pathimagevarian'=>'required',
            'berat'=>'required',
            'lebar'=>'required',
            'panjang'=>'required',
            'tinggi'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $product['id_user']=$input['id_user'];
        $product['nama']=$input['nama'];
        $product['harga']=$input['harga'];
        $product['deskripsi']=$input['deskripsi'];
        $product['stock']=$input['stock'];
        $product['kondisi']=$input['kondisi'];
        $product['berbahaya']=$input['berbahaya'];
        $products = Product::create($product);
        $id_product=$products->id_product;
        $imageproduct['id_product']=$id_product;
        $imageproduct['id_user']=$input['id_user'];
        // if ($request->hasfile('pathimage')) {
        //     $images = $request->file('images');

        //     foreach($images as $image) {
        //         $name = $image->getClientOriginalName();
        //         $path = $image->storeAs('uploads', $name, 'public');

        //         Image::create([
        //             'name' => $name,
        //             'path' => '/storage/'.$path
        //           ]);
        //     }
        //  }


    }
    public function all(){
        $product=DB::table('products')
        // ->select('products.*','user_details.kota',DB::raw("AVG(ratings.rating) AS rating"))
        ->select('products.*','user_details.kota')
        ->join('user_details', 'products.id_user', '=', 'user_details.id_user')
        // ->leftJoin('ratings', 'products.id_product', '=', 'ratings.id_product')
        ->get();
        $request=[
            'Produk'=>$product
        ];
        return response()->json($request,200); 
    }
    public function top(Request $request){
        $validator = Validator::make($request->all(), [
            'start'=>'required',
            'limit'=>'required'
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $product=DB::table('products')
        ->select('products.*','wishes.id_wish','user_details.name as toko','user_details.image as imagetoko',
        DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=products.id_user and alamat_users.utama='ya') as kota"),
        DB::raw("(select pathimage from product_images where product_images.id_product=products.id_product and product_images.sampul='ya') as image"),
        DB::raw("(select AVG(rating) from ratings where ratings.id_product=products.id_product) as rating")
        )
        ->join('user_details','products.id_user','=','user_details.id_user')
        ->leftJoin('wishes','products.id_product','=','wishes.id_product')
        ->orderBy('products.terjual','DESC')
        ->offset($input['start'])
        ->limit($input['limit'])
        ->get();
        $request=[
            'Produk'=>$product
        ];
        return response()->json($request,200); 
    }
    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'start'=>'required',
            'limit'=>'required',
            'keyword'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $product=DB::table('products');


    
        $product=$product->leftjoin('product_images', 'products.id_product', '=', 'product_images.id_product')
        ->leftjoin('alamat_users', 'products.id_user', '=', 'alamat_users.id_user')
        ->leftJoin('wishes','products.id_product','=','wishes.id_product')
        ->join('user_details','products.id_user','=','user_details.id_user')
        ->where('products.nama','LIKE','%'.$input['keyword'].'%');
      
        // ->leftJoin('ratings', 'products.id_product', '=', 'ratings.id_product')
        
        //filter
        if(!empty($request->get('kota'))){

            $provinsi=$request->get('kota');
            $product->where(function($query) use ($provinsi){
                for($i=0; $i<count($provinsi); $i++){
                    if($i==0){
                        $query->where('provinsi','LIKE','%'.$provinsi[$i].'%');
                    }
                    else{
                        $query->orWhere('provinsi','LIKE','%'.$provinsi[$i].'%');
                    }
                }
                
                
            });
        }
        if(!empty($input['min']) || !empty($input['max'])){
            if($input['min']<=0){
                $input['min']=0;
            }
            if($input['max']<=0){
                $input['max']=0;
            }
            $min=$input['min'];
            $max=$input['max'];
            $product->where(function($query) use ($min,$max){
                $query->where('products.harga', '>=', $min);
                $query->where('products.harga', '<=', $max);
            });
        }
        if(!empty($input['urutan'])){
            if($input['urutan']=='tertinggi'){
                $product->orderBy('products.harga','DESC');
            }
            else{
                $product->orderBy('products.harga','ASC');
            }
        }
        else{
            $product->orderBy('products.terjual','DESC');
            
        }
        $kota=$product;
        $kota=$kota->groupBy('kota');
        $product->limit($input['limit'])->offset($input['start']);
        // $product=
        
        $request=[
            'Produk'=>$product->select('products.*','wishes.id_wish','user_details.name as toko','user_details.image as imagetoko',
            DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=products.id_user and alamat_users.utama='ya') as kota"),
            DB::raw("(select pathimage from product_images where product_images.id_product=products.id_product and product_images.sampul='ya') as image"),
            DB::raw("(select AVG(rating) from ratings where ratings.id_product=products.id_product) as rating"))->get(),
            'Provinsi'=>$kota->select(DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=products.id_user and alamat_users.utama='ya') as kota"))->get()
        ];
        return response()->json($request,200); 
   
        
    }
    
    public function detail(Request $request){
        $validator = Validator::make($request->all(), [
            'id_product'=>'required',
            'id_user'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        //image
        $image=DB::table('product_images')
        ->select('pathimage','sampul')
        ->where('id_product',$input['id_product'])
        ->get();
        //otherproduct
        $product=DB::table('products')
        ->select('products.*','wishes.id_wish','user_details.name as toko','user_details.image as imagetoko',
        DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=products.id_user and alamat_users.utama='ya') as kota"),
        DB::raw("(select pathimage from product_images where product_images.id_product=products.id_product and product_images.sampul='ya') as image"),
        DB::raw("(select AVG(rating) from ratings where ratings.id_product=products.id_product) as rating")
        )
        ->join('user_details','products.id_user','=','user_details.id_user')
        ->leftJoin('wishes','products.id_product','=','wishes.id_product')
        ->orderBy('products.terjual','DESC')
        ->where('products.id_product','!=',$input['id_product'])
        ->where('products.id_user',$input['id_user'])
        ->limit(12)
        ->get();
        //ratingreview
        $rata=DB::table('ratings')
        ->select('rating')
        ->where('id_product',$input['id_product'])
        ->avg('rating');
        $total=DB::table('ratings')
        ->select('rating')
        ->where('id_product',$input['id_product'])
        ->count('rating');
        $userrating=DB::table('ratings')
        ->select('ratings.rating','user_details.image','user_details.name','ratings.deskripsi')
        ->leftjoin('user_details', 'ratings.id_user', '=', 'user_details.id_user')
        ->where('id_product',$input['id_product'])
        ->orderBy('rating','DESC')
        ->limit(1)
        ->get();
        //Toko
        $toko=DB::table('user_details')
        ->select('user_details.name','user_details.image',
        DB::raw("(select alamat_users.provinsi from alamat_users where alamat_users.id_user=user_details.id_user and alamat_users.utama='ya') as kota"))
        ->where('user_details.id_user',$input['id_user'])
        ->first();
        $rating=array(
            'rating'=>$rata,
            'total'=>$total
        );
        $respon=array(
            'Produk'=>$product,
            'ImageProduct'=>$image,
            'Rating'=>[$rating],
            'UserRating'=>$userrating,
            'Toko'=>$toko
        );
        return response()->json($respon,200); 

    }
    public function varian(Request $request){
        $validator = Validator::make($request->all(), [
            'id_product'=>'required',
            'bintang'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $varian=DB::table('product_varians')
        ->select('*')
        ->where('id_product',$input['id_product'])
        ->get();
        $respon=array(
            'Varian'=>$varian,
        );
        return response()->json($respon,200);

    }
    public function rating(Request $request){
        $validator = Validator::make($request->all(), [
            'id_product'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $userrating=DB::table('ratings')
        ->select('ratings.rating','user_details.image','user_details.name','ratings.deskripsi')
        ->leftjoin('user_details', 'ratings.id_user', '=', 'user_details.id_user')
        ->where('id_product',$input['id_product'])
        ->where('rating',$input['bintang'])
        ->orderBy('rating','DESC')
        ->get();
        $respon=array(
            'UserRating'=>$userrating,
        );
        return response()->json($respon,200);
    }
}
