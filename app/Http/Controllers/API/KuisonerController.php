<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Kuisoner;
use App\Models\User;
use App\Models\wfh;
use App\Models\user_detail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Excel;
use Illuminate\Support\Str;
use App\Mail\Download;
use App\Exports\ExportExcel;
use Illuminate\Support\Facades\Mail;

$random = Str::random(40);

class KuisonerController extends BaseController
{
    public function mingguan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggalawal' => 'required',
            'tanggalakhir' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $startDate = $input['tanggalawal'];
        $endDate = $input['tanggalakhir'];
  
        $kuisoners = Kuisoner::select('*')
                        ->whereDate('created_at', '>=', $startDate)
                        ->whereDate('created_at', '<=', $endDate)
                        ->where('id_user',$input['id_user'])
                        ->orderBy('created_at',"ASC")
                        ->get();
        if(!empty($kuisoners)){

        
        $response=array(
            'kuisoner'=>$kuisoners
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200); 
        
    }
    public function hariini(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggal' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $tanggal = $input['tanggal'];
  
        $kuisoners = Kuisoner::select('*')
                        ->whereDate('created_at', '=', $tanggal)
                        ->where('id_user',$input['id_user'])
                        ->get();
        if(!empty($kuisoners)){

        
        $response=array(
            'kuisoner'=>$kuisoners
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200); 
    }
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'p1' => 'required',
            'p3' => 'required',
            'p4' => 'required',


        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $kuisoner= kuisoner::create($input);
        if(!empty($kuisoner)){
            $response = array(
                'success'=>true
            );
        }
        else{
            $response=array(
                'message'=>"Gagal DItambah"
            );
        }
        return response()->json($response,200); 
    }
    public function pesan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'tanggalawal' => 'required',
            'tanggalakhir' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $startDate = $input['tanggalawal'];
        $endDate = $input['tanggalakhir'];
  
        $kuisoners = Kuisoner::select(DB::raw("COUNT(p1) as olahraga"),DB::raw("SUM(p6) as minum"))
                        ->whereDate('created_at', '>=', $startDate)
                        ->whereDate('created_at', '<=', $endDate)
                        ->where('p1','ya')
                        ->where('id_user',$input['id_user'])
                        ->first();
        // $kursi = Kuisoner::select(DB::raw("COUNT(p7) as kursi"))
        //                 ->whereDate('created_at', '>=', $startDate)
        //                 ->whereDate('created_at', '<=', $endDate)
        //                 ->where('p7','kursi')
        //                 ->where('id_user',$input['id_user'])
        //                 ->first();
        // $lesehan = Kuisoner::select(DB::raw("COUNT(p7) as lesehan"))
        //                 ->whereDate('created_at', '>=', $startDate)
        //                 ->whereDate('created_at', '<=', $endDate)
        //                 ->where('p7','lesehanmeja')
        //                 ->where('id_user',$input['id_user'])
        //                 ->first();
        // $tanpa = Kuisoner::select(DB::raw("COUNT(p1) as tanpa"))
        //                 ->whereDate('created_at', '>=', $startDate)
        //                 ->whereDate('created_at', '<=', $endDate)
        //                 ->where('p7','lesehantanpameja')
        //                 ->where('id_user',$input['id_user'])
        //                 ->first();
        // if($kursi['kursi']>$lesehan && $)
        if(!empty($kuisoners)){
        $message='';

        if($kuisoners['minum']>14000){
            $minum="Kebutuhan Air Putih Anda Sudah Tercukupi, ";
        }
        else{
            $air=(14000-$kuisoners['minum'])/1000;
            $minum="Cukupi Kebutuhan Air Putih Anda dengan minum sebanyak ".$air." Liter, ";
        }
        if($kuisoners['olahraga']>2){
            $olahraga="Anda Telah Melakukan Olahraga Selama ".$kuisoners['olahraga']." Hari";
        }
        else{
            $olahraga="Anda Kurang Berolahraga";
        }
        
        $response=array(
            'olahraga'=>$olahraga,
            'minum'=>$minum,
            'duduk'=>"perhatikan posisi duduk Anda sehingga terhindar dari risiko akibat bahaya ergonomis (saraf terjepit, nyeri otot pinggang,punggung, dan lainnya."
        );
        }
        else{
            $response=array(
                'message'=>'Tidak Ada Data Pada Tanggal Yang Anda Pilih'
            );
        }   
        return response()->json($response,200); 




    }
    public function request(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'email' => 'required',
        ]);

   
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        $kuisoners = User::select('level')
                        ->where('id',$input['id_user'])
                        ->first();
        
        
        if(!empty($kuisoners)){
            if($kuisoners['level']==1){
                
                $randomid = Str::random(40);
                $user=DB::table('users')
                ->where('id', $input['id_user'])
                ->update(['download' => $randomid]);
                $data=Mail::to($input['email'])->send(new Download($randomid));
                $response=array(
                    'success'=>true
                );
                
            }
            else{
                $response=array(
                    'message'=>"Gagal Mengirim Pesan"
                );
            }
        }  
        else{
            $response=array(
                'message'=>"Gagal Mengirim Pesan"
            );
        }
        return response()->json($response,200); 
    }
    public function download($id){
        if(!empty($id)){

            return Excel::download(new ExportExcel($id), 'Data-Kuisoner.xlsx');
        }
    }
}
