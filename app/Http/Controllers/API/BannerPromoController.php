<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\BannerPromo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Validator;

class BannerPromoController extends BaseController
{
    //
    public function index(){
        $slider = BannerPromo::all();
        $response=array();
        foreach ($slider as $s) {
        $response[]=array(
            "url"   =>$s->url,
            "hdpi"  => url('/storage/upload/'.$s->hdpi),
            'mdpi'  => url('/storage/upload/'.$s->mdpi),
            'xhdpi'  => url('/storage/upload/'.$s->xhdpi),
            'xxhdpi'  => url('/storage/upload/'.$s->xxhdpi),
            'xxxhdpi'  => url('/storage/upload/'.$s->xxxhdpi)
            );
            
        }
        $akhir=[
            'Slider'=>$response
        ]; 
        if (!$slider->isEmpty()){
            return response()->json($akhir, 200);
            }
            else {
                return $this->sendRes('Tidak Ada Data');
            } 
    }
    public function tambah(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user'=>'required',
            'banner'=>'required'
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),200);       
        }
        $input = $request->all();
        if($request->banner) {

            $image = $request->file('banner');
            $input['banner'] = time().$image->getClientOriginalName();
         
            $destinationPath = public_path('/storage/upload/');
            $img = Image::make($image->path());
            $img->resize(480, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."mdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(800, 480, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."hdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(960, 640, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xhdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(1600, 960, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xxhdpi-".$input['banner']);
            $img = Image::make($image->path());
            $img->resize(1920, 960, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'."xxxhdpi-".$input['banner']);


            $destinationPath = public_path('/storage/upload');
            $image->move($destinationPath, $input['banner']);
            $data['mdpi']= "mdpi-".$input['banner'];
            $data['hdpi']="hdpi-".$input['banner'];
            $data['xhdpi']="xhdpi-".$input['banner'];
            $data['xxhdpi']="xxhdpi-".$input['banner'];
            $data['xxxhdpi']="xxxhdpi-".$input['banner'];
            
            $Images=BannerPromo::create($data);
            
            
        }
    }


    public function createThumbnail($path, $width, $height)
{
    // $img = Image::make($path)->resize($width, $height, function ($constraint) {
    //     $constraint->aspectRatio();
    // });
    $img = Image::make($path)->resize($width, $height)->save($path);
    $img->save($path);
}
}
