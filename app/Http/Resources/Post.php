<?php
  
namespace App\Http\Resources;
   
use Illuminate\Http\Resources\Json\JsonResource;
   
class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_post' => $this->id_post,
            'id_forum' => $this->id_forum,
            'id_user' => $this->id_user,
            'name' => $this->name,
            'konten' => $this->konten,
            'vote' => $this->vote,
            'like' => $this->like,
            'komentar' => $this->komentar,

        ];
    }
}