<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Download extends Mailable
{
    public $kode;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ko)
    {
        //
        $this->kode=$ko;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('download')
        ->subject('Link Download Data Kuisoner') // <- just add this line
        ->with([
            
            'Kode' => $this->kode
        ]);
    }
}
