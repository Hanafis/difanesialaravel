<?php
namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Kuisoner;
use App\Models\User;
use App\Models\wfh;
use App\Models\user_detail;
use DB;
class ExportExcel implements FromCollection,WithHeadings
{
    use Exportable;
    public function __construct(string $id)
    {
        $this->id = $id;
    }
    public function collection()
    {
        $user=User::select('download')
        ->where('download',$this->id)
        ->first();
        if(!empty($user)){
            $kuisoner=Kuisoner::select('*','kuisoners.created_at as tanggal_update')
                    ->join('user_details', 'kuisoners.id_user', '=', 'user_details.id_user')
                    ->get()
                    ->toArray();
            $customer_array[] = array();
            $index=0;
            foreach($kuisoner as $customer)
            {
                $tanggal=date("Y-m-d",strtotime($customer['tanggal_update']));
                $wfh=Wfh::select('jenis','jam')
                ->whereDate('created_at',$tanggal)
                    ->first();
                    if(empty($wfh)){
                        $wfh=array(
                            'jenis'=>null,
                            'jam'=>null,
                        );
                    }
            $customer_array[] = array(
            'Customer Name'  => $customer['name'],
            'Jenis Kelamin'   => $customer['jenis_kelamin'],
            'Tanggal Lahir'    => $customer['tanggal_lahir'],
            'Nomor Telephon'  => $customer['no_tlpn'],
            'Jenis Aktifitas'=>$wfh['jenis'],
            'Total Jam Beraktifitas'=>$wfh['jam'],
            'Tanggal Isi Kuisoner'   => $customer['tanggal_update'],
            // "BMI"=>$BMI,
            "Pertanyaan 1"=>$customer['p1'],
            "Pertanyaan 2"=>$customer['p2'],
            "Pertanyaan 3"=>$customer['p3'],
            "Pertanyaan 4"=>$customer['p4'],
            "Pertanyaan 5"=>$customer['p5'],
            "Pertanyaan 6"=>$customer['p6'],
            );
            $index++;
            }
            
        }
        return collect($customer_array);
    }
     public function headings(): array
    {
        return [
            "Nama Lengkap",
            "Jenis Kelamin",
            "Tanggal Lahir",
            "Nomor Telephon",
            "Jenis Aktifitas",
            "Total Jam Beraktifitas",
            " Tanggal Isi Kuisoner",
            "BMI","Apakah Hari Ini Anda Olahraga",
            "Apakah Jenis olah raga yang dilakukan hari ini? ",
            "Apakah anda mengonsumsi makanan yang mengandung vitamin?",
            "Apakah anda mengonsumsi Buah?",
            "Mengandung Vitamin apa makanan/buah yang anda makan?",
            "Saya bekerja dengan posisi?"
        ];
    }
}