<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductOngkirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_ongkirs', function (Blueprint $table) {
            $table->increments('id_ongkir');
            $table->integer('id_product');
            $table->integer('id_user');
            $table->string('berat');
            $table->string('lebar');
            $table->string('panjang');
            $table->string('tinggi');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_ongkirs');
    }
}
