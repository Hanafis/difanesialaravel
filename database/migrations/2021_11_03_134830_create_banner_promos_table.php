<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_promos', function (Blueprint $table) {
            $table->increments('id_banner');
            $table->string('mdpi');
            $table->string('hdpi');
            $table->string('xhdpi');
            $table->string('xxhdpi');
            $table->string('xxxhdpi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_promos');
    }
}
