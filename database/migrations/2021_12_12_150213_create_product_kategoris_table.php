<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductKategorisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_kategoris', function (Blueprint $table) {
            $table->increments('id_kategori_product');
            $table->integer('id_product');
            $table->integer('id_kategori');
            $table->integer('id_sub_kategori');
            $table->integer('id_user');
            $table->string('nama_kategori');
            $table->string('nama_sub_kategori');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_kategoris');
    }
}
