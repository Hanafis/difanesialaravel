<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVarianFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_varian_fotos', function (Blueprint $table) {
            $table->increments('id_varian_foto');
            $table->integer('id_product');
            $table->integer('id_user');
            $table->string('pathimage');
            $table->string('jenis');
            $table->string('varian');
            $table->string('harga');
            $table->string('stock');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_varian_fotos');
    }
}
