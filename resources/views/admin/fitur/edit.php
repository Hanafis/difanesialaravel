<input type="text" hidden value="<?php echo $fitur ?>" id="fitur">
<input type="text" hidden value="<?php echo $path_kategori?>" id="path_kategori">
<?php 
echo form_open_multipart($path.'edit/'.$data->id);
?> 
                <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Add New Post
                    </h2>
                    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
<!--                         <button type="submit" name="preview" class="button box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0"> <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview </button> -->
                        <div class="dropdown">
                            <button type="submit" class="dropdown-toggle button text-white bg-theme-1 shadow-md flex items-center"> Save </button>
                        </div>
                    </div>
                </div>
                <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
                    <!-- BEGIN: Post Content -->
                    <div class="intro-y col-span-12 lg:col-span-8">
                        <input type="text" name="judul" required class="form-control intro-y input input--lg w-full box pr-10 placeholder-theme-13" placeholder="Title" value="<?php echo $data->judul ?>">
                        <div class="post intro-y overflow-hidden box mt-5">
                            <div class="post__tabs nav-tabs flex flex-col sm:flex-row bg-gray-300 dark:bg-dark-2 text-gray-600">
                                <a title="Fill in the article content" data-toggle="tab" data-target="#content" href="javascript:;" class="text-white tooltip w-full sm:w-40 py-4 text-center flex justify-center items-center active"> <i data-feather="file-text" class="text-white w-4 h-4 mr-2"></i> <span class="text-white" >Content</span> </a>
                            </div>
                            <div class="post__content tab-content">
                                <div class="tab-content__pane p-5 active" id="content">
                                    <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5"> <i data-feather="chevron-down" class="text-white w-4 h-4 mr-2"></i> Text Content </div>
                                        <div class="mt-5">
                                            <textarea class="editor1" id="editor1" name="deskripsi"><?php echo $data->deskripsi?></textarea>
                                        </div>
                                    </div>
                                    <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 mt-5">
                                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5"> <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Caption & Images </div>
                                        <div class="mt-5">
                                            <div>
                                                <label>Caption</label>
                                                <input type="text" class="input w-full border mt-2" name="alt" placeholder="Write caption" value="<?php echo $data->alt ?>">
                                            </div>
                                            <div class="mt-3">
                                                <label>Upload Image (450 x 335) Pixel</label>
                                                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                                                    <div class="flex flex-wrap px-4">
                                                        <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                                            <img class="rounded-md" id="blah" src="<?php echo base_url('assets/upload/image/thumbs/').$data->gambar?>">
                                                        </div>
                                                    </div>
                                                    <div class="px-4 pb-4 flex items-center cursor-pointer relative">
                                                        <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 
                                                        <input type="file" onchange="readURL(this);" name="file" class="w-full h-full top-0 left-0 absolute opacity-0">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    <!-- Modul-->
                                  <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 mt-5">

                                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5"> <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Modul Direktori (Abaikan Jika Modul Tidak Inggin di Ganti) </div>

                                        <div class="mt-5">

                                            <div>

                                                <label>Nama Modul</label>

                                                <input type="text" class="input w-full border mt-2" name="altmodul" placeholder="Write caption" value="<?php echo $data->altmodul ?>">

                                            </div>
                                            <div>
                                                
                                            </div>

                                            <div class="mt-3">

                                                <label>Upload Modul PDF,DOC,DOCX</label>

                                                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4 button bg-theme-1 text-white">

                                                    <div class="flex flex-wrap px-4">


                                                    </div>

                                                    <div class="px-4 pb-4 flex items-center cursor-pointer relative ">

                                                        <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1"></span> 

                                                        <input type="file"  name="modul" class="w-full h-full top-0 left-0 absolute ">

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>
                            <!-- End Modul-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Post Content -->
                    <!-- BEGIN: Post Info -->
                    <div class="col-span-12 lg:col-span-4">
                        <div class="intro-y box p-5">
                            <div>
                                <label>Written By</label>
                                     <div class="mt-2"> 
                                        <select data-search="true" class="tail-select w-full" name="author">
                                            <?php foreach ($user as $user) {?>
                                             <option value="<?php echo $user->id_user?>" <?php if($data->id_user==$user->id_user){echo "selected";}?>><?php echo $user->nama_depan." ".$user->nama_belakang?></option>
                                         <?php } ?>
                                         </select> 
                                     </div>
                            </div>
                            <div class="mt-3">
                                <label>Post Date</label>
                                <input class="datepicker input w-full border mt-2" data-single-mode="true" name="tanggal" value="<?php echo date("d M, Y",strtotime($data->tanggal))?>">
                            </div>
                            <div class="mt-3">
                                <?php if($site_map=="Direktori"){?>
                                <label>Wilayah</label>
                                <div class="mt-2">
                                   
                                    <select data-placeholder="Select Countries" class="select-negara w-full" name="negara" id="select-appende" >
                                        <?php foreach ($negara as $negara) {
                                        ?> 
                                        <option value="<?php echo $negara->nama_negara ?>" <?php if ($data->negara==$negara->nama_negara) { ?> selected <?php } ?>><?php echo $negara->nama_negara ?></option>
                                        <?php }?>
                                    </select>
                                    <div style="padding-bottom: 5%;"></div>
                                    </div>
                                    
                                <label>Tipe</label>
                                    <div class="mt-2">
                                    
                                   <select data-placeholder="Select Countries" class="select-negara w-full" name="tipe" id="select-appende" >
                                    <option value="Perorangan"<?php if ($data->tipe=="Perorangan") { ?> selected <?php } ?>>Perorangan</option>
                                    <option value="Organisasi"<?php if ($data->tipe=="Organisasi") { ?> selected <?php } ?>>Organisasi</option>
                                   </select>
                                    <div style="padding-bottom: 5%;"></div>
                                    </div>
                                    <?php } ?>
                                <label>Categories</label>
                                <div class="mt-2">
                                    
                                        <input class=" input w-full border " style="width:60%; float: left;" data-single-mode="true" id="kategori">
                                        <div onclick="kategoridata()" style="float: left; margin-left: 5%; margin-bottom: 7%;" class="btn-kategori dropdown-toggle button text-white bg-theme-1 shadow-md flex items-center">Tambah</div>
                                   
                                    <select data-placeholder="Select categories" class="select-kategori w-full" multiple name="kategori[]" id="select-append" required="">
                                        <?php foreach ($kategori as $kategori) {
                                        ?> 
                                        <option value="<?php echo $kategori->nama_kategori; ?>" <?php foreach ($list_kategori as $list) if($kategori->nama_kategori==$list->nama_kategori){echo "Selected";}?>><?php echo $kategori->nama_kategori; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                
                                <div class="mt-2">
                                <input type="text" name="facebook" placeholder="Link Facebook" class=" input w-full border mt-2" value="<?php echo $data->facebook ?>">
                                </div>
                                <div class="mt-2">
                                <input type="text" name="twitter" placeholder="Link Twitter" class=" input w-full border mt-2" value="<?php echo $data->twitter ?>">
                                </div>
                                <div class="mt-2">
                                <input type="text" name="instagram" placeholder="Link Instagram" class=" input w-full border mt-2" value="<?php echo $data->instagram ?>">
                                </div>
                                <div class="mt-2">
                                <input type="text" name="youtube" placeholder="Link Youtube" class=" input w-full border mt-2" value="<?php echo $data->youtube ?>">
                                </div>
                            
                            </div>
                            <div class="mt-3">
                                <label>Published</label>
                                <div class="mt-2">
                                    <input class="input input--switch border" type="checkbox" name="publish" value="publish" <?php if($data->publish==1){echo "checked";} ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Post Info -->
                </div>
            </div>
            </form>
            <!-- END: Content -->
            <?php echo form_close(); ?>
            <script type="text/javascript">
                     function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                                
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
            </script>
            <script>

    CKEDITOR.replace('editor1' ,{
        filebrowserImageBrowseUrl : '<?php echo base_url('assets/filemanager/index.html');?>'
    });
            </script>