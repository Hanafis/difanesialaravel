<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
          
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Web Konfigurasi
                    </h2>
                     <?php

                              if($this->session->flashdata('warning')){
                                echo '<p>';
                                echo $this->session->flashdata('warning');
                                echo '</p>';
                              }

                              //notifikasi logout
                              if($this->session->flashdata('sukses')){
                                echo '<p>';
                                echo $this->session->flashdata('sukses');
                                echo '</p>';
                              }
                            ?>

                </div>
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Profile Menu -->
                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                        <div class="intro-y box mt-5">
                            <div class="relative flex items-center p-5">
                                <div class="w-12 h-12 image-fit">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$this->session->userdata('gambar')?>">
                                </div>
                                <div class="ml-4 mr-auto">
                                    <div class="font-medium text-base"><?php echo $this->session->userdata('nama_depan')." ".$this->session->userdata('nama_belakang');?></div>
                                    <div class="text-gray-600"><?php echo $this->session->userdata('akses_level')?></div>
                                </div>
                            </div>
                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/');?>"><i data-feather="activity" class="w-4 h-4 mr-2"></i> Basic Information </a>
                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="<?php echo base_url('admin/web/slider/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Beranda </a>
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/about/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Tentang Kami </a>
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/mitra/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Mitra Utama </a>
                            </div>
                        </div>
                    </div>
                    <!-- END: Profile Menu -->
                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                        <!-- BEGIN: Display Information -->
                           <?php 
                    echo form_open_multipart($path.'slider');
                    ?>
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Slider 1 Pada Home (1920 x 935)
                                    <?php echo validation_errors('<div class="alert alert-warning">','</div>');  ?>
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                            <div class=" relative image-fit cursor-pointer zoom-in mx-auto" style="height: 20rem;">
                                                <img id="blah" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$web->beranda?>" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div>
                                            <label>Caption Gambar</label>
                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Caption Gambar" value="<?php echo $web->alt ?>" name="alt"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="text" name="gambar" id="gambar" hidden="" value="a">
                        <div class="px-4 pb-4 flex items-center cursor-pointer relative">
                            <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 
                            <input type="file" onchange="readURL(this);" name="file" class="w-full h-full top-0 left-0 absolute opacity-0">
                        </div>
                        <div class="flex justify-end mt-4">
                            <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                        </div>
                        <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                        </div>
                    <?php echo form_close(); ?>
                           <?php 
                    echo form_open_multipart($path.'slider1');
                    ?>
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Slider 2 Pada Home (1920 x 935)
                                    <?php echo validation_errors('<div class="alert alert-warning">','</div>');  ?>
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                            <div class=" relative image-fit cursor-pointer zoom-in mx-auto" style="height: 20rem;">
                                                <img id="blah1" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$web->beranda1?>" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div>
                                            <label>Caption Gambar</label>
                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Caption Gambar" value="<?php echo $web->alt1 ?>" name="alt1"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="text" name="gambar1" id="gambar1" hidden="" value="a">
                        <div class="px-4 pb-4 flex items-center cursor-pointer relative">
                            <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 
                            <input type="file" onchange="readURL1(this);" name="files" class="w-full h-full top-0 left-0 absolute opacity-0">
                        </div>
                        <div class="flex justify-end mt-4">
                            <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                        </div>
                        <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                        </div>
                     <?php echo form_close(); ?>
                           <?php 
                    echo form_open_multipart($path.'slider2');
                    ?>
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                   Slider 3 Pada Home (1920 x 935)
                                    <?php echo validation_errors('<div class="alert alert-warning">','</div>');  ?>
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                            <div class=" relative image-fit cursor-pointer zoom-in mx-auto" style="height: 20rem;">
                                                <img id="blah2" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$web->beranda2?>" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-12">
                                        <div>
                                            <label>Caption Gambar</label>
                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Caption Gambar" value="<?php echo $web->alt2 ?>" name="alt2" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="text" name="gambar2" id="gambar2" hidden="" value="a">
                        <!-- END: Display Information -->
                        <div class="px-4 pb-4 flex items-center cursor-pointer relative">
                            <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 
                            <input type="file" onchange="readURL2(this);" name="filed" class="w-full h-full top-0 left-0 absolute opacity-0">
                        </div>
                        <div class="flex justify-end mt-4">
                            <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                        </div>
                        <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                        </div>
                     <?php echo form_close(); ?>
                    </div>
                    
                </div>

            <!-- END: Content -->
            <?php echo form_close(); ?>
            <script type="text/javascript">
                     function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                                    document.getElementById("gambar").value='ada';
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    function readURL1(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah1')
                                    .attr('src', e.target.result);
                                    document.getElementById("gambar1").value='ada';
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    function readURL2(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah2')
                                    .attr('src', e.target.result);
                                    document.getElementById("gambar2").value='ada';
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
            </script>
