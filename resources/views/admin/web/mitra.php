<input type="text" hidden value="<?php echo base_url()?>" id="base_url">

<input type="text" hidden value="<?php echo $path?>" id="path_list">

                           <?php 

                    echo form_open_multipart($path.'mitra');

                    ?> 

                    <input type="text" name="cek" hidden="" value="cek" required="">         

                <!-- END: Top Bar -->

                <div class="intro-y flex items-center mt-8">

                    <h2 class="text-lg font-medium mr-auto">

                        Web Konfigurasi

                    </h2>

                     <?php



                              if($this->session->flashdata('warning')){

                                echo '<p>';

                                echo $this->session->flashdata('warning');

                                echo '</p>';

                              }



                              //notifikasi logout

                              if($this->session->flashdata('sukses')){

                                echo '<p>';

                                echo $this->session->flashdata('sukses');

                                echo '</p>';

                              }

                            ?>

                </div>

                <div class="grid grid-cols-12 gap-6">

                    <!-- BEGIN: Profile Menu -->

                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">

                        <div class="intro-y box mt-5">

                            <div class="relative flex items-center p-5">

                                <div class="w-12 h-12 image-fit">

                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$this->session->userdata('gambar')?>">

                                </div>

                                <div class="ml-4 mr-auto">

                                    <div class="font-medium text-base"><?php echo $this->session->userdata('nama_depan')." ".$this->session->userdata('nama_belakang');?></div>

                                    <div class="text-gray-600"><?php echo $this->session->userdata('akses_level')?></div>

                                </div>

                            </div>

                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/');?>"><i data-feather="activity" class="w-4 h-4 mr-2"></i> Basic Information </a>

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/slider/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Beranda </a>

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/about/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Tentang Kami </a>

                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="<?php echo base_url('admin/web/mitra/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Mitra Utama </a>

                            </div>

                        </div>

                    </div>



                    <!-- END: Profile Menu -->

                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">

                        <!-- BEGIN: Display Information -->

                        <div class="intro-y box lg:mt-5">

                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                                <h2 class="font-medium text-base mr-auto">

                                    Mitra Utama 1

                                </h2>

                            </div>

                            <div class="p-5">

                                <div class="grid grid-cols-12 gap-5">

                                    <div class="col-span-12 xl:col-span-4">

                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">

                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

                                                <img id="satu" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$utama[0]->gambar?>" >



                                               <!--  <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-span-12 xl:col-span-8">

                                          <div class="mt-3">

                                            <label>Nama Mitra</label>

                                            <select class="input w-full border mt-2" name="id_mitra[]" id="mitra" onchange="mymitra();">
                                                <option></option>
                                                <?php foreach ($mitra as $mitra) {

                                                ?>
                                                
                                                <option value="<?php echo $mitra->id_mitra?>" data-gambar="<?php echo $mitra->gambar?>" data-link="<?php echo $mitra->link?>" data-deskripsi="<?php echo $mitra->deskripsi?>" <?php if ($mitra->id_mitra==$utama[0]->id_mitra){?> selected <?php } ?>><?php echo $mitra->nama?></option>

                                                <?php } ?>

                                            </select>

                                        </div>

                                        

                                    </div>

                                </div>

                            </div>

                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                                <h2 class="font-medium text-base mr-auto">

                                    Mitra Utama 2

                                </h2>

                            </div>

                            <div class="p-5">

                                <div class="grid grid-cols-12 gap-5">

                                    <div class="col-span-12 xl:col-span-4">

                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">

                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

                                                <img id="dua" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$utama[1]->gambar?>" >



                                               <!--  <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-span-12 xl:col-span-8">

                                          <div class="mt-3">

                                            <label>Nama Mitra</label>
                                            
                                            <select class="input w-full border mt-2" name="id_mitra[]" id="mitra1" onchange="mymitra1();">
                                                <option></option>
                                                <?php foreach ($mitra1 as $mitra) {

                                                ?>

                                                <option value="<?php echo $mitra->id_mitra?>" data-gambar="<?php echo $mitra->gambar?>" data-link="<?php echo $mitra->link?>" data-deskripsi="<?php echo $mitra->deskripsi?>" <?php if ($mitra->id_mitra==$utama[1]->id_mitra){?> selected <?php } ?>><?php echo $mitra->nama?></option>

                                                <?php } ?>

                                            </select>

                                        </div>



                                        

                                    </div>

                                </div>

                            </div>

                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                                <h2 class="font-medium text-base mr-auto">

                                    Mitra Utama 3

                                </h2>

                            </div>

                            <div class="p-5">

                                <div class="grid grid-cols-12 gap-5">

                                    <div class="col-span-12 xl:col-span-4">

                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">

                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

                                                <img id="tiga" class="rounded-md" alt="" src="<?php echo base_url('assets/upload/image/thumbs/').$utama[2]->gambar?>" >



                                               <!--  <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-span-12 xl:col-span-8">

                                          <div class="mt-3">

                                            <label>Nama Mitra</label>

                                            <select class="input w-full border mt-2" name="id_mitra[]" id="mitra2" onchange="mymitra2();">
                                                <option></option>
                                                <?php foreach ($mitra2 as $mitra) {

                                                ?>

                                                <option value="<?php echo $mitra->id_mitra?>" data-gambar="<?php echo $mitra->gambar?>" data-link="<?php echo $mitra->link?>" data-deskripsi="<?php echo $mitra->deskripsi?>" <?php if ($mitra->id_mitra==$utama[2]->id_mitra){?> selected <?php } ?>><?php echo $mitra->nama?></option>

                                                <?php } ?>

                                            </select>

                                        </div>

                                        

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- END: Display Information -->

                        <div class="flex justify-end mt-4">

                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>

                            </div>

                                <div class="flex justify-end mt-4" style="margin-bottom: 5%;">

                                </div>

                    </div>

                    

                </div>

                <?php echo form_close(); ?>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

            <script type="text/javascript">

                                function mymitra() {

                                

                                    var sel = document.getElementById('mitra');

                                    var selected = sel.options[sel.selectedIndex];

                                    var gambar= '<?php echo base_url('assets/upload/image/thumbs/')?>'+selected.getAttribute('data-gambar');

                                    $('#satu').attr('src', gambar);

                                }

                                function mymitra1() {

                                    var sel = document.getElementById('mitra1');

                                    var selected = sel.options[sel.selectedIndex];

                                    var gambar= '<?php echo base_url('assets/upload/image/thumbs/')?>'+selected.getAttribute('data-gambar');

                                    $('#dua').attr('src', gambar);

                                }

                                function mymitra2() {

                                    var sel = document.getElementById('mitra2');

                                    var selected = sel.options[sel.selectedIndex];

                                    var gambar= '<?php echo base_url('assets/upload/image/thumbs/')?>'+selected.getAttribute('data-gambar');

                                    $('#tiga').attr('src', gambar);                                

                                }

                                

            </script>

