<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                           <?php 
                    echo form_open_multipart($path.'about');
                    ?>          
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Web Konfigurasi
                    </h2>
                     <?php

                              if($this->session->flashdata('warning')){
                                echo '<p>';
                                echo $this->session->flashdata('warning');
                                echo '</p>';
                              }

                              //notifikasi logout
                              if($this->session->flashdata('sukses')){
                                echo '<p>';
                                echo $this->session->flashdata('sukses');
                                echo '</p>';
                              }
                            ?>

                </div>
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Profile Menu -->
                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                        <div class="intro-y box mt-5">
                            <div class="relative flex items-center p-5">
                                <div class="w-12 h-12 image-fit">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$this->session->userdata('gambar')?>">
                                </div>
                                <div class="ml-4 mr-auto">
                                    <div class="font-medium text-base"><?php echo $this->session->userdata('nama_depan')." ".$this->session->userdata('nama_belakang');?></div>
                                    <div class="text-gray-600"><?php echo $this->session->userdata('akses_level')?></div>
                                </div>
                            </div>
                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/');?>"><i data-feather="activity" class="w-4 h-4 mr-2"></i> Basic Information </a>
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/slider/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Beranda </a>
                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="<?php echo base_url('admin/web/about/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Tentang Kami </a>
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/mitra/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Mitra Utama </a>
                            </div>
                        </div>
                    </div>

                    <!-- END: Profile Menu -->
                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                        <!-- BEGIN: Personal Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    About Information
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                       <div class="mt-3">
                                            <label>Visi</label>
                                            <textarea  class="editor" name="visi"><?php echo $web->visi ?></textarea>
                                        </div>
                                        <div class="mt-3">
                                            <label>Misi</label>
                                            <textarea  class="editor" name="misi"><?php echo $web->misi ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                       <div class="mt-3">
                                            <label>Deskripsi</label>
                                            <textarea  class="editor" name="deskripsi"><?php echo $web->deskripsi ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-end mt-4">
                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                            </div>
                                <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Personal Information -->
                    </div>
                    
                </div>
                <?php echo form_close(); ?>

