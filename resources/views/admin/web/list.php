<input type="text" hidden value="<?php echo base_url()?>" id="base_url">

<input type="text" hidden value="<?php echo $path?>" id="path_list">

                           <?php 

                    echo form_open_multipart($path);

                    ?>          

                <!-- END: Top Bar -->

                <div class="intro-y flex items-center mt-8">

                    <h2 class="text-lg font-medium mr-auto">

                        Web Konfigurasi

                    </h2>

                     <?php



                              if($this->session->flashdata('warning')){

                                echo '<p>';

                                echo $this->session->flashdata('warning');

                                echo '</p>';

                              }



                              //notifikasi logout

                              if($this->session->flashdata('sukses')){

                                echo '<p>';

                                echo $this->session->flashdata('sukses');

                                echo '</p>';

                              }

                            ?>

                </div>

                <div class="grid grid-cols-12 gap-6">

                    <!-- BEGIN: Profile Menu -->

                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">

                        <div class="intro-y box mt-5">

                            <div class="relative flex items-center p-5">

                                <div class="w-12 h-12 image-fit">

                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$this->session->userdata('gambar')?>">

                                </div>

                                <div class="ml-4 mr-auto">

                                    <div class="font-medium text-base"><?php echo $this->session->userdata('nama_depan')." ".$this->session->userdata('nama_belakang');?></div>

                                    <div class="text-gray-600"><?php echo $this->session->userdata('akses_level')?></div>

                                </div>

                            </div>

                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">

                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium" href="<?php echo base_url('admin/web/');?>"><i data-feather="activity" class="w-4 h-4 mr-2"></i> Basic Information </a>

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/slider/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Beranda </a>

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/about/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Tentang Kami </a>

                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/web/mitra/');?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Mitra Utama </a>

                            </div>

                        </div>

                    </div>



                    <!-- END: Profile Menu -->

                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">

                        <!-- BEGIN: Display Information -->

                        <div class="intro-y box lg:mt-5">

                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                                <h2 class="font-medium text-base mr-auto">

                                    Basic Information

                                </h2>

                            </div>

                            <div class="p-5">

                                <div class="grid grid-cols-12 gap-5">

                                    <div class="col-span-12 xl:col-span-4">

                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">

                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">

                                                <img class="rounded-md" alt="Midone Tailwind HTML Admin Template" id="blah" src="<?php echo base_url('assets/upload/image/thumbs/').$web->logo?>">

                                               <!--  <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->

                                            </div>

                                            <div class="w-40 mx-auto cursor-pointer relative mt-5">

                                                <button type="button" class="button w-full bg-theme-1 text-white">Change Logo</button>

                                                <input type="file" onchange="readURL(this); "class="w-full h-full top-0 left-0 absolute opacity-0" name="file" >

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-span-12 xl:col-span-8">

                                        <div>

                                            <label>Nama Website</label>

                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Nama Website" value="<?php echo $web->nama ?>" name="nama" required="" >

                                        </div>

                                        <div>

                                            <label>Pemilik Website</label>

                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Pemilik Website" value="<?php echo $web->author?>" name="author">

                                        </div>

                                        <div>

                                            <label>Keyword</label>

                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="keyword" value="<?php echo $web->keyword ?>" name="keyword">

                                        </div>

                                        

                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- END: Display Information -->

                        <!-- BEGIN: Personal Information -->

                        <div class="intro-y box lg:mt-5">

                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">

                                <h2 class="font-medium text-base mr-auto">

                                    Personal Information

                                </h2>

                            </div>

                            <div class="p-5">

                                <div class="grid grid-cols-12 gap-5">

                                    <div class="col-span-12 xl:col-span-6">

                                        <div>

                                            <label>Email</label>

                                            <input type="text"   class="input w-full border bg-gray-100  mt-2" placeholder="Email website" value="<?php echo $web->email ?>" name="email">

                                        </div>

                                        <div>

                                            <label>Nomor Telpon</label>

                                            <input type="number"   class="input w-full border mt-2" placeholder="Nomor Telpon" value="<?php echo $web->no_tlpn ?>" name="no_tlpn"> 

                                        </div> 

                                       <div class="mt-3">

                                            <label>Alamat</label>

                                            <textarea  class="input w-full border mt-2" name="alamat" style="height: 140px;"><?php echo $web->alamat ?></textarea>

                                        </div>

                                    </div>

                                    <div class="col-span-12 xl:col-span-6">

 

                                        <div class="mt-3">

                                            <label>Facebook</label>

                                            <input type="text" class="input w-full border mt-2" placeholder="Facebook" value="<?php echo $web->facebook  ?>" name="facebook">

                                        </div>

                                        <div class="mt-3">

                                            <label>Twitter</label>

                                            <input type="text"  class="input w-full border mt-2" placeholder="Twitter" value="<?php echo $web->twitter ?>" name="twitter">

                                        </div>

                                        <div class="mt-3">

                                            <label>Instagram</label>

                                            <input type="text" re class="input w-full border mt-2" placeholder="Instagram" value="<?php echo $web->instagram ?>" name="instagram">

                                        </div>
                                        <div class="mt-3">
                                            <label>Youtube</label>
                                            <input type="text" re class="input w-full border mt-2" placeholder="Youtube" value="<?php echo $web->youtube ?>" name="youtube">
                                        </div>
                                    </div>

                                </div>

                                <div class="flex justify-end mt-4">

                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>

                            </div>

                                <div class="flex justify-end mt-4" style="margin-bottom: 5%;">

                                </div>

                                </div>

                            </div>

                        </div>

                        <!-- END: Personal Information -->

                    </div>

                    

                </div>

                <?php echo form_close(); ?>

            <script type="text/javascript">

                     function readURL(input) {

                        if (input.files && input.files[0]) {

                            var reader = new FileReader();



                            reader.onload = function (e) {

                                $('#blah')

                                    .attr('src', e.target.result);

                            };



                            reader.readAsDataURL(input.files[0]);

                        }

                    }

            </script>

