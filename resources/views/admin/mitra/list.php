<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        List <?php echo $site_map?>
                    </h2>
                    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
                        <button onclick="javascript:location.href=`<?php echo $path.'tambah'?>`" class="button text-white bg-theme-1 shadow-md mr-2">Tambah <?php echo $site_map?></button>
                    </div>
                </div>
                <!-- BEGIN: HTML Table Data -->
                <div class="intro-y box p-5 mt-5">
                    <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
                        <form class="xl:flex sm:mr-auto" id="tabulator-list-mitra-html-filter-form">
                            <div class="sm:flex items-center sm:mr-4">
                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Field</label>
                                <select class="input w-full sm:w-32 xxl:w-full mt-2 sm:mt-0 sm:w-auto border" id="tabulator-list-mitra-html-filter-field">
                                    <option value="judul">Judul</option>
                                </select>
                            </div>
                            <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">
                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Type</label>
                                <select class="input w-full mt-2 sm:mt-0 sm:w-auto border" id="tabulator-list-mitra-html-filter-type">
                                    <option value="like" selected>like</option>
                                    <option value="=">=</option>
                                    <option value="<">
                                        <</option>
                                            <option value="<=">
                                                <=</option>
                                                    <option value=">">></option>
                                                    <option value=">=">>=</option>
                                                    <option value="!=">!=</option>
                                                </select>
                                            </div>
                                            <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">
                                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Value</label>
                                                <input type="text" class="input w-full sm:w-40 xxl:w-full mt-2 sm:mt-0 border" id="tabulator-list-mitra-html-filter-value" placeholder="Search...">
                                            </div>
                                            <div class="mt-2 xl:mt-0">
                                                <button type="button" class="button w-full sm:w-16 bg-theme-1 text-white" id="tabulator-list-mitra-html-filter-go">Go</button>
                                                <button type="button" class="button w-full sm:w-16 mt-2 sm:mt-0 sm:ml-1 bg-gray-200 text-gray-600 dark:bg-dark-5 dark:text-gray-300" id="tabulator-list-mitra-html-filter-reset">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="overflow-x-auto scrollbar-hidden">
                                        <div class="mt-5 table-report table-report--tabulator" id="tabulator-list-mitra"></div>
                                    </div>
                                </div>
                                <!-- END: HTML Table Data -->
                            </div>
                            <!-- END: Content -->

<div class="modal delete_modal" id="delete-modal-preview">
    <div class="modal__content"> 
        <div class="p-5 text-center"> 
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
            <div class="text-3xl mt-5">Delete Artikel Ini ...?</div> 
            <div class="text-gray-600 mt-2">Apakah Anda Benar Inggin menghapus Artikel ini? </div> 
        </div> 
        <div class="px-5 pb-8 text-center"> 
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">
                Batal
            </button> 
            <a id="delete_id_button">
                <button  type="button" class="button w-24 bg-theme-6 text-white">
                Delete
                </button>
            </a>
        </div>
    </div>
</div> 