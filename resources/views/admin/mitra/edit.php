

<?php 

echo form_open_multipart($path.'edit/'.$mitra->id);

?> 

<div> 

	<label>Nama Mitra</label> 

	<input type="text" class="input w-full border mt-2" placeholder="Masukan Nama Mitra" name="nama" required="" value="<?php echo $mitra->nama?>"> 

</div>
<div> 

	<label>Link Website/Sosial Media Mitra</label> 

	<input type="text" class="input w-full border mt-2" placeholder="Masukan Link Website / Sosial Media" name="link" required="" value="<?php echo $mitra->link?>"> 

</div>

<div> 

	<label>Deskripsi Mitra</label> 

	<textarea class="input w-full border mt-2" placeholder="Masukan Deskripsi Mitra" name="deskripsi" ><?php echo $mitra->deskripsi?></textarea> 

</div>


                                    <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 mt-5">

                                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5"> <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Logo Mitra </div>

                                        <div class="mt-5">

                                            <div>

                                                <label>Caption</label>

                                                <input type="text" class="input w-full border mt-2" name="alt" placeholder="Write caption" value="<?php echo $mitra->alt ?>" >

                                            </div>

                                            <div class="mt-3">

                                                <label>Upload Image(150 x 150) Pixel</label>

                                                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">

                                                    <div class="flex flex-wrap px-4">

                                                        <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">

                                                            <img class="rounded-md" id="blah" src="<?php echo base_url('assets/upload/image/thumbs/').$mitra->gambar?>">

                                                        </div>

                                                    </div>

                                                    <div class="px-4 pb-4 flex items-center cursor-pointer relative">

                                                        <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 

                                                        <input type="file" onchange="readURL(this);" name="file" class="w-full h-full top-0 left-0 absolute opacity-0">

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

<button type="submit" class="button bg-theme-1 text-white mt-5">Edit</button>

 

 <?php echo form_close(); ?>

             <script type="text/javascript">

                     function readURL(input) {

                        if (input.files && input.files[0]) {

                            var reader = new FileReader();



                            reader.onload = function (e) {

                                $('#blah')

                                    .attr('src', e.target.result);

                            };



                            reader.readAsDataURL(input.files[0]);

                        }

                    }

            </script>