<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                           <?php 
                    echo form_open_multipart($path.'password/'.$user->id_user);
                    ?>          
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Update Profile
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Profile Menu -->
                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                        <div class="intro-y box mt-5">
                            <div class="relative flex items-center p-5">
                                <div class="w-12 h-12 image-fit">
                                    <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$user->gambar?>">
                                </div>
                                <div class="ml-4 mr-auto">
                                    <div class="font-medium text-base"><?php echo $user->nama_depan." ".$user->nama_belakang;?></div>
                                    <div class="text-gray-600"><?php echo $user->level?></div>
                                </div>
                            </div>
                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium" href="<?php echo base_url('admin/user/profil/').$user->id_user?>"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Personal Information </a>
                                <a class="flex items-center mt-5" href=""> <i data-feather="lock" class="w-4 h-4 mr-2"></i> Change Password </a>
                            </div>
                        </div>
                    </div>
 <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                        <!-- BEGIN: Personal Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Ganti Password
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div>
                                            <label>Password Baru</label>
                                            <input type="Password"  class="input w-full border bg-gray-100  mt-2" placeholder="Password Baru" value="<?php echo set_value('password') ?>" name="password">
                                        </div>
                                        <div>
                                            <label>Comfrim Password</label>
                                            <input type="Password"  class="input w-full border bg-gray-100  mt-2" placeholder="Ulangi Password" value="<?php echo set_value('password_confrim') ?>" name="password_confrim" >
                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-end mt-4">
                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                                </div>
                            </div>
                        </div>
                        <!-- END: Personal Information -->
                    </div>
                    
                </div>
                <?php echo form_close(); ?>

