<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                           <?php 
                    echo form_open_multipart($path.'profil/'.$user->id_user);
                    ?>          
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Update Profile
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Profile Menu -->
                    <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                        <div class="intro-y box mt-5">
                            <div class="relative flex items-center p-5">
                                <div class="w-12 h-12 image-fit">
                                    <img alt="" class="rounded-full" src="<?php echo base_url('assets/upload/image/thumbs/').$user->gambar?>">
                                </div>
                                <div class="ml-4 mr-auto">
                                    <div class="font-medium text-base"><?php echo $user->nama_depan." ".$user->nama_belakang;?></div>
                                    <div class="text-gray-600"><?php echo $user->level?></div>
                                </div>
                            </div>
                            <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                                <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium" href=""> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Personal Information </a>
                                <a class="flex items-center mt-5" href="<?php echo base_url('admin/user/password/').$user->id_user?>"> <i data-feather="lock" class="w-4 h-4 mr-2"></i> Change Password </a>
                            </div>
                        </div>
                    </div>

                    <!-- END: Profile Menu -->
                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                        <!-- BEGIN: Display Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Display Information
                                </h2>
                    <?php echo validation_errors('<div class="alert alert-warning">','</div>');   ?> 
                    <?php 
                            if($this->session->flashdata('password')){
                                echo '<p>';
                                echo $this->session->flashdata('password');
                                echo '</p>';
                                $this->session->set_flashdata('password', ' ');
                              } 
                            if($this->session->flashdata('username')){
                                echo '<p>';
                                echo $this->session->flashdata('username');
                                echo '</p>';
                                $this->session->set_flashdata('username', ' ');
                              }
                              
                    ?>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-4">
                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                                <img class="rounded-md" alt="" id="blah" src="<?php echo base_url('assets/upload/image/thumbs/').$user->gambar?>">
                                                <!-- <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->
                                            </div>
                                            <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                                <button type="button" class="button w-full bg-theme-1 text-white">Change Photo</button>
                                                <input type="file" onchange="readURL(this); "class="w-full h-full top-0 left-0 absolute opacity-0" name="file" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-8">
                                        <div>
                                            <label>Nama Depan</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Nama Depan" value="<?php echo $user->nama_depan ?>" name="nama_depan">
                                        </div>
                                        <div>
                                            <label>Nama Belakang</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Nama Belakang" value="<?php echo $user->nama_belakang ?>" name="nama_belakang">
                                        </div>
                                        <div>
                                            <label>Email</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Email" value="<?php echo $user->email ?>" name="email">
                                        </div>
                                        <div>
                                            <label>Donasi/Member</label>
                                            <select class="input w-full border mt-2" name="member" >
                                                <option value="ya" <?php if ($user->member=="ya") { ?> selected <?php } ?>>YA</option>
                                                <option value="tidak" <?php if ($user->member=="tidak") { ?> selected <?php } ?>>TIDAK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Display Information -->
                        <!-- BEGIN: Personal Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Personal Information
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div>
                                            <label>Username</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Username" value="<?php echo $user->username ?>" name="username">
                                        </div>
                                        <div>
                                            <label>Negara</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Negara" value="<?php echo $user->negara ?>" name="negara" >
                                        </div>
                                          <div class="mt-3">
                                            <label>Tipe</label>
                                            <select class="input w-full border mt-2" name="type" >
                                                <option value="Perorangan" <?php if ($user->type=="Perorangan"){ ?> Selected <?php } ?> >Perorangan</option>
                                                <option value="Organisasi" <?php if ($user->type=="Organisasi"){ ?> Selected <?php } ?> >Organisasi</option>
                                            </select>
                                        </div>
                                        <div class="mt-3">
                                            <label>Level</label>
                                            <select class="input w-full border mt-2" name="level">
                                                <option value="Admin" <?php if ($user->level=="Admin"){ ?> Selected <?php } ?> >Admin</option>
                                                <option value="User"<?php if ($user->level=="User"){ ?> Selected <?php } ?> >User</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label>Nomor Telpon</label>
                                            <input type="number" required=""  class="input w-full border mt-2" placeholder="Nomor Telpon" value="<?php echo $user->no_tlpn ?>" name="no_tlpn"> 
                                        </div> 
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="mt-3">
                                            <label>Alamat</label>
                                            <input type="text" required=""  class="input w-full border mt-2" placeholder="Alamat" value="<?php echo $user->alamat ?>" name="alamat">
                                        </div>
                                        <div class="mt-3">
                                            <label>Facebook</label>
                                            <input type="text" class="input w-full border mt-2" placeholder="Facebook" value="<?php echo $user->facebook  ?>" name="facebook">
                                        </div>
                                        <div class="mt-3">
                                            <label>Twitter</label>
                                            <input type="text"  class="input w-full border mt-2" placeholder="Twitter" value="<?php echo $user->twitter ?>" name="twitter">
                                        </div>
                                        <div class="mt-3">
                                            <label>Instagram</label>
                                            <input type="text" re class="input w-full border mt-2" placeholder="Instagram" value="<?php echo $user->instagram ?>" name="instagram">
                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-end mt-4">
                                    <a href="#" onclick="deletefunction(<?php echo $user->id_user ?>);return false;" data-toggle="modal" data-target="#delete-modal-preview" class="text-theme-6 flex items-center"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete Account </a>
                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                                                                    </div>
                                <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Personal Information -->
                    </div>
                    
                </div>
                <?php echo form_close(); ?>
 <div class="modal delete_modal" id="delete-modal-preview">
    <div class="modal__content"> 
        <div class="p-5 text-center"> 
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
            <div class="text-3xl mt-5">Delete User Ini ...?</div> 
            <div class="text-gray-600 mt-2">Apakah Anda Benar Inggin menghapus User ini? </div> 
        </div> 
        <div class="px-5 pb-8 text-center"> 
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">
                Batal
            </button> 
            <a id="delete_id_button">
                <button  type="button" class="button w-24 bg-theme-6 text-white">
                Delete
                </button>
            </a>
        </div>
    </div>
</div> 
            <script type="text/javascript">
                     function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
            </script>
