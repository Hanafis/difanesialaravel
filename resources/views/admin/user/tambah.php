                           <?php 
                    echo form_open_multipart($path.'tambah');
                    ?>          
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Tambah User
                    </h2>

                </div>

                    <!-- END: Profile Menu -->
                    <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                        <!-- BEGIN: Display Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Display Information
                                </h2>
                    <?php echo validation_errors('<div class="alert alert-warning">','</div>');   ?> 
                    <?php 
                            if($this->session->flashdata('password')){
                                echo '<p>';
                                echo $this->session->flashdata('password');
                                echo '</p>';
                                $this->session->set_flashdata('password', ' ');
                              } 
                            if($this->session->flashdata('username')){
                                echo '<p>';
                                echo $this->session->flashdata('username');
                                echo '</p>';
                                $this->session->set_flashdata('username', ' ');
                              }
                    ?>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-4">
                                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                                <img class="rounded-md" alt="" id="blah" src="<?php echo base_url('assets/admin/dist/images/profile-7.jpg')?>">
                                               <!--  <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->
                                            </div>
                                            <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                                <button type="button" class="button w-full bg-theme-1 text-white">Change Photo</button>
                                                <input type="file" onchange="readURL(this); "class="w-full h-full top-0 left-0 absolute opacity-0" name="file" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-8">
                                        <div>
                                            <label>Nama Depan</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Nama Depan" value="<?php echo set_value('nama_depan') ?>" name="nama_depan">
                                        </div>
                                        <div>
                                            <label>Nama Belakang</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Nama Belakang" value="<?php echo set_value('nama_belakang') ?>" name="nama_belakang">
                                        </div>
                                        <div>
                                            <label>Email</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Email" value="<?php echo set_value('email') ?>" name="email">
                                        </div>
                                        <div>
                                            <label>Donasi/Member</label>
                                            <select class="input w-full border mt-2" name="member" >
                                                <option value="ya">YA</option>
                                                <option value="tidak">TIDAK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Display Information -->
                        <!-- BEGIN: Personal Information -->
                        <div class="intro-y box lg:mt-5">
                            <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Personal Information
                                </h2>
                            </div>
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div>
                                            <label>Username</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Username" value="<?php echo set_value('username') ?>" name="username">
                                        </div>
                                        <div>
                                            <label>Password</label>
                                            <input type="Password"  class="input w-full border bg-gray-100  mt-2" placeholder="Username" value="<?php echo set_value('password') ?>" name="password">
                                        </div>
                                        <div>
                                            <label>Comfrim Password</label>
                                            <input type="Password"  class="input w-full border bg-gray-100  mt-2" placeholder="Username" value="<?php echo set_value('password_confrim') ?>" name="password_confrim" >
                                        </div>
                                        <div>
                                            <label>Negara</label>
                                            <input type="text" required=""  class="input w-full border bg-gray-100  mt-2" placeholder="Negara" value="<?php echo set_value('negara') ?>" name="negara" >
                                        </div>
                                          <div class="mt-3">
                                            <label>Tipe</label>
                                            <select class="input w-full border mt-2" name="type" >
                                                <option value="Perorangan">Perorangan</option>
                                                <option value="Organisasi">Organisasi</option>
                                            </select>
                                        </div>
                                        <div class="mt-3">
                                            <label>Level</label>
                                            <select class="input w-full border mt-2" name="level">
                                                <option value="Admin">Admin</option>
                                                <option value="User">User</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label>Nomor Telpon</label>
                                            <input type="number" required=""  class="input w-full border mt-2" placeholder="Nomor Telpon" value="<?php echo set_value('no_tlpn') ?>" name="no_tlpn"> 
                                        </div> 
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="mt-3">
                                            <label>Alamat</label>
                                            <input type="text" required=""  class="input w-full border mt-2" placeholder="Alamat" value="<?php echo set_value('alamat') ?>" name="alamat">
                                        </div>
                                        <div class="mt-3">
                                            <label>Facebook</label>
                                            <input type="text" class="input w-full border mt-2" placeholder="Facebook" value="<?php echo set_value('facebook') ?>" name="facebook">
                                        </div>
                                        <div class="mt-3">
                                            <label>Twitter</label>
                                            <input type="text"  class="input w-full border mt-2" placeholder="Twitter" value="<?php echo set_value('twitter') ?>" name="twitter">
                                        </div>
                                        <div class="mt-3">
                                            <label>Instagram</label>
                                            <input type="text" re class="input w-full border mt-2" placeholder="Instagram" value="<?php echo set_value('instagram') ?>" name="instagram">
                                        </div>

                                    </div>
                                </div>
                                <div class="flex justify-end mt-4" >
                                    <!-- <a href="" class="text-theme-6 flex items-center"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete Account </a> -->
                                    <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Save</button>
                                </div>
                                <div class="flex justify-end mt-4" style="margin-bottom: 5%;">
                                </div>
                            </div>
                        </div>
                        <!-- END: Personal Information -->
                    </div>
                    
                </div>
                <?php echo form_close(); ?>
            <script type="text/javascript">
                     function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
            </script>