<?php 

$mode=$this->session->userdata('mode');

if ($mode==null or $mode=="light"){

    $mode="light";

}

?>

<!DOCTYPE html>

<html lang="en" class="<?php echo $mode?>">

    <!-- BEGIN: Head -->

    <head>

        <meta charset="utf-8">

        <link href="<?php echo base_url('assets/upload/image/thumbs/').$web->logo;?>" rel="shortcut icon">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Admin Website Jogja Disability Arts.">

        <meta name="keywords" content="Admin Website Jogja Disability Arts.">

        <meta name="author" content="JOGJA Disability ARTS">

        <title>Dashboard Admin JDA Website</title>

        <!-- BEGIN: CSS Assets-->

        <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/app.css')?>" />

        <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/dark.css')?>" />

        <!-- END: CSS Assets-->
        <script src="//cdn.ckeditor.com/4.16.0/standard-all/ckeditor.js"></script>
        <link href="<?php echo base_url('assets/dropzone/dist/dropzone.css')?>" rel="stylesheet">
       
        

    </head>

    <!-- END: Head -->