
                        <!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="text-white-dark"><?php echo $site_map ?></a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="" class="aktif"><?php echo $site_map_detail ?></a> </div>
                    <!-- END: Breadcrumb -->
                    <!-- BEGIN: Account Menu -->
                    <div class="intro-x dropdown w-8 h-8">
                        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in">
                            <img alt="Gambar User" src="<?php echo base_url('assets/upload/image/thumbs/').$this->session->userdata('gambar')?>">
                        </div>
                        <div class="dropdown-box w-56">
                            <div class="dropdown-box__content box bg-theme-38 dark:bg-dark-6 text-white">
                                <div class="p-4 border-b border-theme-40 dark:border-dark-3">
                                    <div class="font-medium text-white"><?php echo $this->session->userdata('nama_depan')." ".$this->session->userdata('nama_belakang');?></div>
                                    <div class="text-xs text-theme-41 mt-0.5 dark:text-gray-600">Login Sebagai <?php echo $this->session->userdata('akses_level')?> </div>
                                </div>
                                <div class="p-2">
                                    <a href="<?php echo base_url('admin/user/profil/').$this->session->userdata('id_user')?>" class="text-white flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Profile </a>
                                    <a href="<?php echo base_url('admin/user/tambah')?>" class="text-white flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="edit" class="w-4 h-4 mr-2"></i> Add Account </a>
                                    <a href="<?php echo base_url('admin/user/password/').$this->session->userdata('id_user')?>" class="text-white flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="lock" class="w-4 h-4 mr-2"></i> Reset Password </a>
                                    <a href="" class="text-white flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="help-circle" class="w-4 h-4 mr-2"></i> Help </a>
                                </div>
                                <div class="text-white p-2 border-t border-theme-40 dark:border-dark-3">
                                    <a href="<?php echo base_url('logout')?>" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Account Menu -->
                </div>