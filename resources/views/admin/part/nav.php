
    <body class="app">
        <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="" class="flex mr-auto">
                    <img alt="Admin JDA WEBSITE " class="w-6" src="<?php echo base_url('assets/upload/image/thumbs/').$web->logo?>" style="background-color: white; border-radius: 50%;">
                </a>
                <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <ul class="border-t border-theme-24 py-5 hidden">
                <li>
                    <a href="<?php echo base_url('admin/dashboard/')?>" class="menu menu--active">
                        <div class="menu__icon"> <i data-feather="home"></i> </div>
                        <div class="menu__title"> Dashboard </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/pembayaran')?>" class="menu menu--active">
                        <div class="menu__icon"> <i data-feather="heart"></i> </div>
                        <div class="menu__title"> Donasi </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="folder"></i> </div>
                        <div class="menu__title"> Direktori <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="<?php echo base_url('admin/direktori')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> List Direktori </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/direktori/tambah')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Tambah Direktori </div>
                            </a>
                        </li>
                    </ul>
                </li>
                                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="award"></i> </div>
                        <div class="menu__title"> Program <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="<?php echo base_url('admin/program')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> List Program </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/program/tambah')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Tambah Program </div>
                            </a>
                        </li>

                    </ul>
                </li>
                                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="clipboard"></i> </div>
                        <div class="menu__title"> Acara <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="<?php echo base_url('admin/acara')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> List Acara </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/acara/tambah')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Tambah Acara </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="bookmark"></i> </div>
                        <div class="menu__title"> Berita <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="<?php echo base_url('admin/berita')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> List Berita </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/berita/tambah')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Tambah Berita </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu__devider my-6"></li>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> Users <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="<?php echo base_url('admin/user')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> List Users </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/user/tambah')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Add Users </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/user/profil/').$this->session->userdata('id_user')?>" class="menu">
                                <div class="menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="menu__title"> Profil Users </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/web')?>" class="menu menu--active">
                        <div class="menu__icon"> <i data-feather="settings"></i> </div>
                        <div class="menu__title"> Web Konfigurasi </div>
                    </a>
                </li>
            </ul>
        </div>
        <!-- END: Mobile Menu -->
        <div class="flex">
            <!-- BEGIN: Side Menu -->
            <nav class="side-nav">
                <a href="" class="intro-x flex items-center pl-5 pt-4">
                    <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="<?php echo base_url('assets/upload/image/thumbs/').$web->logo?>" style="background-color: white; border-radius: 50%;">
                    <span class="hidden text-white xl:block text-lg ml-3"> JDA<span class="font-medium"> Yogyakarta</span> </span>
                </a>
                <div class="side-nav__devider my-6"></div>
                <ul>
                    <li>
                        <a href="<?php echo base_url("admin/dashboard") ?>" class="side-menu side-menu <?php if ($site_map=="Dashboard"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url("admin/pembayaran") ?>" class="side-menu side-menu <?php if ($site_map=="Donasi"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="heart"></i> </div>
                            <div class="side-menu__title"> Donasi </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Direktori"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="folder"></i> </div>
                            <div class="side-menu__title">
                                Direktori 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/direktori')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Direktori </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/direktori/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Direktori </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Program"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="award"></i> </div>
                            <div class="side-menu__title">
                                Program 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/program')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Program </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/program/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Program </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Acara"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="clipboard"></i> </div>
                            <div class="side-menu__title">
                                Acara 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/acara')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Acara </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/acara/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Acara </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Berita"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="bookmark"></i> </div>
                            <div class="side-menu__title">
                                Berita 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/berita')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Berita </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/berita/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Berita </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Mitra"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="box"></i> </div>
                            <div class="side-menu__title">
                                Mitra 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/mitra')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Mitra </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/mitra/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Mitra </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="Gallery"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="box"></i> </div>
                            <div class="side-menu__title">
                                Gallery 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/gallery')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List Gallery </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/gallery/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Tambah Gallery </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="side-nav__devider my-6"></li>
                    <li>
                        <a href="javascript:;" class="side-menu <?php if ($site_map=="User"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                            <div class="side-menu__title">
                                Users 
                                <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="<?php echo base_url('admin/user')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> List User </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/user/tambah')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Add User </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/user/profil/').$this->session->userdata('id_user')?>" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> User Profil </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url("admin/web") ?>" class="side-menu side-menu <?php if ($site_map=="Web"){echo $aktif;}?>">
                            <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                            <div class="side-menu__title"> Web Konfigurasi </div>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- END: Side Menu -->