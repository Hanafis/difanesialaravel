        <?php 

        if ($mode=="light"){

        

        ?>

        <!-- BEGIN: Dark Mode Switcher-->

        <div data-url="<?php echo base_url('mode')?>" class="dark-mode-switcher cursor-pointer shadow-md fixed bottom-0 right-0 box dark:bg-dark-2 border rounded-full w-40 h-12 flex items-center justify-center z-50 mb-10 mr-10">

            <div class="mr-4 text-gray-700 dark:text-gray-300">Dark Mode</div>

            <div class="dark-mode-switcher__toggle border"></div>

        </div>

    <?php } else { ?>

        <div data-url="<?php echo base_url('mode')?>" class="dark-mode-switcher cursor-pointer shadow-md fixed bottom-0 right-0 box dark:bg-dark-2 border rounded-full w-40 h-12 flex items-center justify-center z-50 mb-10 mr-10">

            <div class="mr-4 text-gray-700 dark:text-gray-300">Dark Mode</div>

            <div class="dark-mode-switcher__toggle dark-mode-switcher__toggle--active border"></div>

        </div>

    <?php }?>

        <!-- END: Dark Mode Switcher-->

        <!-- BEGIN: JS Assets-->

       <!--  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script> -->

        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=["your-google-map-api"]&libraries=places"></script> -->

       <script src="https://cdn.jsdelivr.net/npm/tail.select@0.5.15/js/tail.select-full.min.js"></script>

       <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.1.4/dist/js/tabulator.min.js"></script>

       <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.9/xlsx.min.js"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/app.js')?>"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/list.js')?>"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/mitra.js')?>"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/pembayaran.js')?>"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/user.js')?>"></script>

        <script src="<?php echo base_url('assets/admin/dist/js/kategori.js')?>"></script>

        

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/dropzone/dist/dropzone.js')?>"></script>
                            <script type="text/javascript">
                                var errors = false;
                                Dropzone.autoDiscover = false;
                                $(".dropzone").dropzone({
                                 addRemoveLinks: true,
                                 removedfile: function(file) {
                                   var name = file.name; 
                                   
                                   $.ajax({
                                     type: 'POST',
                                     url: 'upload',
                                     data: {name: name,request: 2},
                                     sucess: function(data){
                                        console.log('success: ' + data);
                                     }
                                   });
                                   var _ref;
                                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                                 },
                                 error: function(file, errorMessage) {
                                    errors = true;
                                },
                                 queuecomplete: function(file) {
                                        if(errors)  {
                                            $("#erroralert").css("display", "block");
                                            $("#suksesalert").css("display", "none");
                                        }
                                        else {
                                            $("#erroralert").css("display", "none");
                                            $("#suksesalert").css("display", "block");
                                        }
                                    }
                                });
                            </script>

        <!-- END: JS Assets-->

    </body>

</html>