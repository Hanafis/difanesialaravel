 <input type="text" name="path" value="<?php echo base_url('admin/gallery/')?>" id="path" hidden>
 <!-- BEGIN: Weekly Top Products -->
                        <div class="col-span-12 mt-6">
                            <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        List <?php echo $site_map?>
                    </h2>
                    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
                        <button onclick="javascript:location.href=`<?php echo $path.'tambah'?>`" class="button text-white bg-theme-1 shadow-md mr-2">Tambah <?php echo $site_map?></button>
                    </div>
                </div>
                            <div class="intro-y overflow-auto lg:overflow-visible mt-8 sm:mt-0">
                            <table class="table table-report sm:mt-2">
                                 <thead>
                                     <tr>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Gambar</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Deskripsi</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Edit</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Hapus</th>
                                     </tr>
                                 </thead>
                                 <tbody id="load_data">
                                    <?php $no=1; foreach ($gallery as $gallery){ ?>
                                        
                                    
                                     <tr>
                                         <td class="border-b whitespace-nowrap"><?php echo $no; ?></td>
                                         <td class="border-b whitespace-nowrap"><div>
                                        <div class="intro-x " style="width: 100%;">
                                         <img  src="<?php echo base_url('assets/upload/image/thumbs/').$gallery->gambar?>">
                                        </div></td>
                                         <td class="border-b whitespace-nowrap">
                                            <?php echo $gallery->alt;?>
                                            
                         
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/gallery/edit/').$gallery->id_gallery?>"  class="button bg-theme-1 text-white mt-5" style="margin-right: 5%;" >Edit</a>
                                        </td>
                                        <td>
                                             <a href="#" onclick="hapus(<?php echo $gallery->id_gallery?>)" class="button bg-theme-1 text-white mt-5" style="margin-right: 5%;" data-toggle="modal" data-target="#delete-modal-preview">Hapus</a>
                                        </td>
                                     </tr>
                                 <?php $no=$no+1; } ?>
                                 <input type="text" name="no" id="no" value="<?php echo $no?>" hidden>
                                 </tbody>
                             </table>
                             <center>
                             <button class="button bg-theme-1 text-white mt-5" type="button" onclick="loadmore()" >Load More</button>
                             </center>
                            </div>

                        </div>
<div class="modal delete_modal" id="delete-modal-preview">
    <div class="modal__content"> 
        <div class="p-5 text-center"> 
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
            <div class="text-3xl mt-5">Delete Gambar Ini ...?</div> 
            <div class="text-gray-600 mt-2">Apakah Anda Benar Inggin menghapus Gambar ini? </div> 
        </div> 
        <div class="px-5 pb-8 text-center"> 
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">
                Batal
            </button> 
            <a id="delete_id_button">
                <a href="#"  type="button" class="button w-24 bg-theme-6 text-white" onclick="sendhapus()">
                Delete
                </button>
            </a>
        </div>
    </div>
</div> 
                        <!-- END: Weekly Top Products -->
                        <script src="<?php echo base_url('assets/admin/dist/js/gallery.js')?>"></script>