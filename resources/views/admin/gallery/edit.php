<input type="text" hidden value="<?php echo $fitur ?>" id="fitur">
<input type="text" hidden value="<?php echo $path_kategori?>" id="path_kategori">
<?php 
echo form_open_multipart($path.'edit/'.$data->id_gallery);
?> 
                <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Edit Gallery
                    </h2>
                    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
<!--                         <button type="submit" name="preview" class="button box text-gray-700 dark:text-gray-300 mr-2 flex items-center ml-auto sm:ml-0"> <i class="w-4 h-4 mr-2" data-feather="eye"></i> Preview </button> -->
                        <div class="dropdown">
                            <button type="submit" class="dropdown-toggle button text-white bg-theme-1 shadow-md flex items-center"> Save </button>
                        </div>
                    </div>
                </div>
                <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
                    <!-- BEGIN: Post Content -->
                    <div class="intro-y col-span-12 lg:col-span-8">
                        <div class="post intro-y overflow-hidden box mt-5">
                            <div class="post__content tab-content">
                                <div class="tab-content__pane p-5 active" id="content">

                                    <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5 mt-5">
                                        <div class="font-medium flex items-center border-b border-gray-200 dark:border-dark-5 pb-5"> <i data-feather="chevron-down" class="w-4 h-4 mr-2"></i> Upload Gallery </div>
                                        <div class="mt-5">
                                            <div>
                                                <label>Deskripsi</label>
                                                <input type="text" class="input w-full border mt-2" name="alt" placeholder="Write caption" value="<?php echo $data->alt ?>">
                                            </div>
                                            <div class="mt-3">
                                                <label>Upload Image PNG,JPEG,JPG</label>
                                                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                                                    <div class="flex flex-wrap px-4">
                                                        <div class="w-24 h-24 relative image-fit mb-5 mr-5 cursor-pointer zoom-in">
                                                            <img class="rounded-md" id="blah" src="<?php echo base_url('assets/upload/image/thumbs/').$data->gambar?>">
                                                        </div>
                                                    </div>
                                                    <div class="px-4 pb-4 flex items-center cursor-pointer relative">
                                                        <i data-feather="image" class="w-4 h-4 mr-2"></i> <span class="text-theme-1 dark:text-theme-10 mr-1">Upload a file</span> or drag and drop 
                                                        <input type="file" onchange="readURL(this);" name="file" class="w-full h-full top-0 left-0 absolute opacity-0">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Post Content -->
                    <!-- BEGIN: Post Info -->
                    <div class="col-span-12 lg:col-span-4">
                        <div class="intro-y box p-5">
                            <div class="mt-3">
                                <label>Categories</label>
                                <div class="mt-2">
                                    
                                        <input class=" input w-full border " style="width:60%; float: left;" data-single-mode="true" id="kategori">
                                        <div onclick="kategoridata()" style="float: left; margin-left: 5%; margin-bottom: 7%;" class="btn-kategori dropdown-toggle button text-white bg-theme-1 shadow-md flex items-center">Tambah</div>
                                   
                                    <select data-placeholder="Select categories" class="select-kategori w-full" multiple name="kategori[]" id="select-append" required="">
                                        <?php foreach ($kategori as $kategori) {
                                        ?> 
                                        <option value="<?php echo $kategori->nama_kategori; ?>" <?php foreach ($list_kategori as $list) if($kategori->nama_kategori==$list->nama_kategori){echo "Selected";}?>><?php echo $kategori->nama_kategori; ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Post Info -->
                </div>
            </div>
            </form>
            <!-- END: Content -->
            <?php echo form_close(); ?>
            <script type="text/javascript">
                     function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                                
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
            </script>
            <script>

    CKEDITOR.replace('editor1' ,{
        filebrowserImageBrowseUrl : '<?php echo base_url('assets/filemanager/index.html');?>'
    });
            </script>