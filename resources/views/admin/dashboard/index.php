<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                <!-- END: Top Bar -->
                <div class="grid grid-cols-12 gap-6">
                    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
                        <!-- BEGIN: General Report -->
                        <div class="col-span-12 mt-8">
                            <div class="intro-y flex items-center h-10">
                                <h2 class="text-lg font-medium truncate mr-5">
                                    General Report
                                </h2>
                                <a href="" class="ml-auto flex text-theme-1 dark:text-theme-10"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
                            </div>
                            <div class="grid grid-cols-12 gap-6 mt-5">
                                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                    <div class="report-box zoom-in">
                                        <div class="box p-5">
                                            <div class="flex">
                                                <i data-feather="user" class="report-box__icon text-theme-10"></i> 
                                            </div>
                                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo $hariini ?></div>
                                            <div class="text-base text-gray-600 mt-1">Pengunjung Hari Ini</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                    <div class="report-box zoom-in">
                                        <div class="box p-5">
                                            <div class="flex">
                                                <i data-feather="user" class="report-box__icon text-theme-11"></i> 
                                            </div>
                                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo $total ?></div>
                                            <div class="text-base text-gray-600 mt-1">Total Pengunjung</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                    <div class="report-box zoom-in">
                                        <div class="box p-5">
                                            <div class="flex">
                                                <i data-feather="user" class="report-box__icon text-theme-12"></i> 
                                            </div>
                                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo $online ?></div>
                                            <div class="text-base text-gray-600 mt-1">Online</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                    <div class="report-box zoom-in">
                                        <div class="box p-5">
                                            <div class="flex">
                                                <i data-feather="user" class="report-box__icon text-theme-9"></i> 
                                            </div>
                                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo $user ?></div>
                                            <div class="text-base text-gray-600 mt-1">Total User</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: General Report -->
                        <!-- BEGIN: Weekly Top Products -->
                        <div class="col-span-12 mt-6">
                            <div class="intro-y block sm:flex items-center h-10">
                                <h2 class="text-lg font-medium truncate mr-5">
                                    Donasi Pembayaran Yang Belum Di Verifikasi
                                </h2>
                            </div>
                            <div class="intro-y overflow-auto lg:overflow-visible mt-8 sm:mt-0">
                            <table class="table table-report sm:mt-2">
                                 <thead>
                                     <tr>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Nama Pembayar</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Nominal</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Metode Pembayaran</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Status Verifikasi</th>
                                         <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Action</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                    <?php $no=1; foreach ($pembayaran as $pembayaran){ ?>
                                        
                                    
                                     <tr>
                                         <td class="border-b whitespace-nowrap"><?php echo $no; ?></td>
                                         <td class="border-b whitespace-nowrap"><?php echo $pembayaran->nama?></td>
                                         <td class="border-b whitespace-nowrap"><?php echo $pembayaran->nominal?></td>
                                         <td class="border-b whitespace-nowrap"><?php echo $pembayaran->metode?></td>
                                         <td class="border-b whitespace-nowrap"><?php echo $pembayaran->verifikasi?></td>
                                         <td class="border-b whitespace-nowrap">

                                            <a href="<?php echo $path.'detail/'.$pembayaran->id_pembayaran?>" class="button bg-theme-1 text-white mt-5" style="margin-right: 5%;">Detail</a>
                         
                                        </td>
                                     </tr>
                                 <?php $no=$no+1; } ?>
                                 </tbody>
                             </table>
                            </div>

                        </div>
                        <!-- END: Weekly Top Products -->
                    </div>
                </div>
