<input type="text" hidden value="<?php echo base_url()?>" id="base_url">
<input type="text" hidden value="<?php echo $path?>" id="path_list">
                           <?php 
                    echo form_open_multipart($path.'detail/'.$pembayaran->id_pembayaran);
                    ?>   
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Detail Donasi
                    </h2>
                </div>
                <!-- BEGIN: Wizard Layout -->
                <div class="intro-y box py-10 sm:py-20 mt-5">
                    <div class="px-5 sm:px-20">
                        <div class="intro-x flex items-center">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">1</button>
                            <div class="font-medium text-base ml-3">Nama Pembayar&nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</div>
                            <div class="font-medium text-base ml-3"><?php echo $pembayaran->nama ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">2</button>
                            <div class="font-medium text-base ml-3">Email Pembayar &nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : </div>
                            <div class="font-medium text-base ml-3"> <?php echo $pembayaran->email ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">3</button>
                            <div class="font-medium text-base ml-3">Bank Pembayar &nbsp;&nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : </div>
                            <div class="font-medium text-base ml-3"> <?php echo $pembayaran->bank ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">4</button>
                            <div class="font-medium text-base ml-3">Nominal Donasi &nbsp;&nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </div>
                            <div class="font-medium text-base ml-3"> <?php if ($pembayaran->matauang=="idr"){ echo 'Rp. '; }else { echo '$ '; } ; echo $pembayaran->nominal ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">3</button>
                            <div class="font-medium text-base ml-3">Metode Pembayaran &nbsp;&nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp; : </div>
                            <div class="font-medium text-base ml-3"> <?php echo $pembayaran->bank ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">3</button>
                            <div class="font-medium text-base ml-3">Tanggal Pembayaran &nbsp;&nbsp;</div>
                            <div class="font-medium text-base ml-3">&nbsp;&nbsp; : </div>
                            <div class="font-medium text-base ml-3"> <?php echo $pembayaran->tanggal ?></div>
                        </div>
                        <div class="intro-x flex items-center mt-5">
                            <button class="w-10 h-10 rounded-full button text-white bg-theme-1">3</button>
                            <div class="font-medium text-base ml-3">Verifikasi Pembayaran &nbsp;&nbsp;</div>
                            <div class="font-medium text-base ml-3"> : </div>
                            <div class="font-medium text-base ml-3"> <?php echo $pembayaran->verifikasi ?> Di Verifikasi</div>
                        </div>
                    </div>
                    <div class="px-5 sm:px-20 mt-10 pt-10 border-t border-gray-200 dark:border-dark-5">
                        <div class="font-medium text-base">Verifikasi Pembayaran</div>
                        <div class="grid grid-cols-12 gap-4 gap-y-5 mt-5">
                            <div class="intro-y col-span-12 sm:col-span-6">
                                <div class="mb-2">Verifikasi</div>
                                <select class="input w-full border flex-1" name="verifikasi">
                                    <option value="Sudah"<?php if ($pembayaran->verifikasi=='Sudah') { echo 'selected'; } ?>>Berhasil/Sudah Diverifikasi</option>
                                    <option value="Belum"<?php if ($pembayaran->verifikasi=='Belum') { echo 'selected'; } ?>>Gagal/Belum Di Verifikasi</option>
                                </select>
                            </div>
                            <div class="intro-y col-span-12 sm:col-span-6">
                                <div class="mb-2">Catatan</div>
                                <textarea class="input w-full border flex-1" name="catatan"><?php echo $pembayaran->catatan ?></textarea>
                            </div>
                            <div class="intro-y col-span-12 sm:col-span-12">
                                <div class="mb-2">Bukti Pembayaran</div>
                                <img src="<?php echo base_url('assets/upload/image/').$pembayaran->gambar?>">
                            </div>

                            <div class="intro-y col-span-12 flex items-center justify-center sm:justify-end mt-5">
                                <button class="button w-24 justify-center block bg-theme-1 text-white ml-2">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Wizard Layout -->
<?php echo form_close(); ?>