<input type="text" hidden value="<?php echo base_url()?>" id="base_url">

<input type="text" hidden value="<?php echo $path?>" id="path_list">

                <div class="intro-y flex flex-col sm:flex-row items-center mt-8">

                    <h2 class="text-lg font-medium mr-auto">

                        List <?php echo $site_map?>

                    </h2>

                </div>

                <!-- BEGIN: HTML Table Data -->

                <div class="intro-y box p-5 mt-5">

                    <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">

                        <form class="xl:flex sm:mr-auto" id="tabulator-list-pembayaran-html-filter-form">

                            <div class="sm:flex items-center sm:mr-4">

                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Field</label>

                                <select class="input w-full sm:w-32 xxl:w-full mt-2 sm:mt-0 sm:w-auto border" id="tabulator-list-pembayaran-html-filter-field">

                                    <option value="judul">Judul</option>

                                </select>

                            </div>

                            <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Type</label>

                                <select class="input w-full mt-2 sm:mt-0 sm:w-auto border" id="tabulator-list-pembayaran-html-filter-type">

                                    <option value="like" selected>like</option>

                                    <option value="=">=</option>

                                    <option value="<">

                                        <</option>

                                            <option value="<=">

                                                <=</option>

                                                    <option value=">">></option>

                                                    <option value=">=">>=</option>

                                                    <option value="!=">!=</option>

                                                </select>

                                            </div>

                                            <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">

                                                <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Value</label>

                                                <input type="text" class="input w-full sm:w-40 xxl:w-full mt-2 sm:mt-0 border" id="tabulator-list-pembayaran-html-filter-value" placeholder="Search...">

                                            </div>

                                            <div class="mt-2 xl:mt-0">

                                                <button type="button" class="button w-full sm:w-16 bg-theme-1 text-white" id="tabulator-list-pembayaran-html-filter-go">Go</button>

                                                <button type="button" class="button w-full sm:w-16 mt-2 sm:mt-0 sm:ml-1 bg-gray-200 text-gray-600 dark:bg-dark-5 dark:text-gray-300" id="tabulator-list-pembayaran-html-filter-reset">Reset</button>

                                            </div>

                                        </form>

                                    </div>

                                    <div class="overflow-x-auto scrollbar-hidden">

                                        <div class="mt-5 table-report table-report--tabulator" id="tabulator-list-pembayaran"></div>

                                    </div>

                                </div>

                                <!-- END: HTML Table Data -->

                            </div>

                            <!-- END: Content -->


