<?php
  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\KuisonerController;
use App\Http\Controllers\API\WfhController;
use App\Http\Controllers\API\WfhjamController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\SliderController;
use App\Http\Controllers\API\BannerPromoController;
use App\Http\Controllers\API\KeranjangProductController;
use App\Http\Controllers\API\WishController;


  
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  
Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);
Route::get('gagal', [RegisterController::class, 'gagal']);
Route::post('reset', [RegisterController::class, 'reset']);
Route::post('kode', [RegisterController::class, 'kode']);
Route::post('password', [RegisterController::class, 'password']);  
Route::get('product/all', [ProductController::class, 'all']);
Route::post('product/top', [ProductController::class, 'top']);
Route::get('slider', [SliderController::class, 'index']);
Route::post('slider/add', [SliderController::class, 'tambah']);
Route::post('banner/add', [BannerPromoController::class, 'tambah']);
Route::get('banner', [BannerPromoController::class, 'index']);
Route::post('product/search', [ProductController::class, 'search']);
Route::post('product/detail', [ProductController::class, 'detail']);
Route::post('product/rating', [ProductController::class, 'rating']);


Route::middleware('auth:api')->group( function () {


    Route::post('user/', [RegisterController::class, 'user']);
    Route::post('update/user/{id}', [RegisterController::class, 'userupdate']);
    Route::post('password/update', [RegisterController::class, 'ubah_password']);
    //Kuisoner Router
    Route::post('mingguan/', [KuisonerController::class, 'mingguan']);
    Route::post('hariini/', [KuisonerController::class, 'hariini']);
    Route::post('tambah/', [KuisonerController::class, 'tambah']);
    Route::post('mingguanjam/', [WfhController::class, 'mingguan']);
    Route::post('listjam/', [WfhjamController::class, 'list']);
    Route::post('tambahjam/', [WfhjamController::class, 'tambah']);
    Route::post('getid/', [WfhjamController::class, 'getid']);
    Route::post('stop/', [WfhjamController::class, 'stop']);
    Route::post('pesan/', [KuisonerController::class, 'pesan']);
    Route::post('request/', [KuisonerController::class, 'request']);
    Route::post('wish/list', [WishController::class, 'list']);
    Route::post('wish/tambah', [WishController::class, 'tambah']);
    Route::post('keranjang/tambah', [KeranjangProductController::class, 'tambah']);
    Route::post('keranjang/kurang', [KeranjangProductController::class, 'kurang']);
    Route::post('keranjang/list', [KeranjangProductController::class, 'list']);
    Route::post('keranjang/hapus', [KeranjangProductController::class, 'hapus']);
    Route::post('keranjang/count', [KeranjangProductController::class, 'count']);
    Route::post('product/varian', [ProductController::class, 'varian']);
    

});